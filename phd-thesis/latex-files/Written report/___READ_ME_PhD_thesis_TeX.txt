READ ME

This folder contains some .tex files of the thesis "Some Mordell--Weil lattices and applications to sphere packings".

The file "PhD___Written_report.tex" can be compiled using LaTeX (e.g., with TexMaker, after installing a TeX distribution for instance via tug.org/mactex/ or miktex.org).

One has to compile once, thus run bibtex twice, makeindex twice (write "makeindex %.nlo -s nomencl.ist -o %.nls -t %.nlg;" in the settings of TexMaker), and then compile twice again. The whole process may take roughly 3 minutes.

Note: the design/layout is not exactly the same as the published version, as some of the personal commands are not currently made available.


Author: 	Gauthier Leterrier
Date: 	April 2023