% !TEX root = PhD___Written_report.tex

%\clearpage\phantomsection % needed for the hyperlinks to work correctly
\chapter{Proof of the upper bound on the Brauer--Siegel ratio}\label{appendix_Brauer_Siegel}

%Research report - review notes on PhD topic.pdf

\counterwithin{dfTemp}{chapter} %https://tex.stackexchange.com/questions/132045/numbering-depth-in-appendix
\numberwithin{equation}{chapter}

\renewcommand{\L}{\mathcal{L}}



In this appendix, we provide a detailed version (with explicit constants) of the proof of the upper bound on the Brauer--Siegel ratio $\BS(E / K) \leq 1 + o(1)$ from \cite{Hindry_Pacheco}, as stated in \cref{thm_upper_bound_BS_Hindry_Pacheco}. We do not claim originality here; the goal is only to make sure that we can safely remove the dependency on $q := |k|$ (the cardinality of the field of constants).

In what follows, we will use the "complex-analytic" version of the L-function $\L(E/K, s) := L(E/K , T = q^{-s})$ as in \cref{rmk_L_functions}; in particular we have $\mathcal{L}^*(E/K) = \log(q)^r L^*(E/K)$ where $r$ is the (analytic) rank of $E/K$. We point out that in \cite[Remark 2.5]{Hindry_Pacheco}, the regulator is defined using the pairing $\log(q) \cdot \innprod{  -, - }_{\NT}$ (see \cref{rmk_normalization_height}), where $\log$ denotes the natural logarithm (in base $e$). %no way to change the base here!
In particular, the Brauer--Siegel ratio of $E/K$ as defined in \cite{Hindry_Pacheco} is equal to $\BS(E/K) + \frac{r \log \log(q)}{\log H(E/K)}$ (using our notation from \cref{df_Brauer_Siegel_and_Szpiro}). %This will not affect the truth of \cref{thm_upper_bound_BS_Hindry_Pacheco}.

The general idea in \cite{Hindry_Pacheco} to get an upper bound on the Brauer–Siegel ratio can be described in five steps (we use the notations from \cref{thm_upper_bound_BS_Hindry_Pacheco}):
\begin{enumerate}
\item Get "easy" upper bounds on $|\log(\L(E / K, s))|$, from Euler product and from Weil conjectures, where $\re(s) = 1+\rho$ or $\re(s) = 3/2 + \rho$ with $\rho \in ]0, 1/3[$. See \cref{Lemma_7_2}.

\item Apply Phragmen–Lindelöf principle on the strip $\{ s \in \C \tq 1 + \rho \leq \re(s) \leq \frac{3}{2} + \rho \}$, where $\rho \in ]0, 1/3[$ (\cref{Lemma_7_3}). In particular, we get an upper bound on $|\log(\L(E/K, s))|$ for $\re(s) = 1 + 2 \rho$.
%https://en.wikipedia.org/wiki/Lindel%C3%B6f%27s_theorem

\item Use the functional equation of $\L(E/K, s)$ to get an upper bound on $|\log(\L(E/K, s))|$ for $\re(s) = 1 - 2 \rho$. Then apply Phragmen–Lindelöf theorem again, on a strip containing $s = 1$. See \cref{Lemma_7_4}.

\item Cauchy integral formula together with Brumer's bound provide an upper bound on $|\L^*(E/K)|$ (\cref{Bound_on_derivative}).

\item Finally, BSD formula (from \cref{conj_BSD_statements}) gives the desired result (\cref{thm_upper_bound_BS_Hindry_Pacheco}).
\end{enumerate}

We start by writing the zeta function of $K = k(C)$ as
$$\zeta_K(s) = \dfrac{ Q_K(q^{-s}) }{ 	(1 - q^{-s})	(1 - q^{1-s})	 },$$
where $Q_K(T) = \prod\limits_{j=1}^{2g} (1 - a_j T) \in \Z[T]$ has degree $2g$, and $|a_j| = q^{1/2}$ for every $j$. 
We have an easy estimate:
\begin{lemma}\label{upper_bound_on_zeta}
Let $s \in \C$ with $\sigma = \re(s) \in ]1, 2[$. Then
\[
|\zeta_K(s)|	\leq	\dfrac{3 \cdot 2^{2g}}{ \sigma - 1 }.
\qedhere
\]
\end{lemma}


%\begin{remark}
%\begin{enumerate}
%\item This bound is conceptually clear : $\zeta_K(s)$ has a pole of order $1$ at $s=1$, i.e. 
%$\zeta_K(s) \sim c_{-1} / (s-1)$ where $s \to 1$ and $c_{-1} := \Res_{s=1}(\zeta_K)$. Thus is $s$ close enough to $1$, then
%$|\zeta_K(s)| 	\leq   \frac{c'}{|s-1|}$ for some $c'>0$.

%\item 
We could replace the constant $3$ in the numerator by any real number 
$c > \max\{\zeta_{\P^1_{\F_2}}(2)	;    \frac{2}{\ln(2)} \}$ $= \max\{8/3, 2 / \ln(2)\} = 2 / \ln(2) \simeq 2.885$.
%
%\qedhere
%\end{enumerate}
%\end{remark}



\begin{proof}[Proof of \cref{upper_bound_on_zeta}]
To begin with, we have 
\[
|Q_K(q^{-s})| = \prod_{j=1}^{2g} |1 - a_j q^{-s}|	\leq (1 + q^{1/2 - \sigma})^{2g} \leq 2^{2g},
\]
since $1/2  -  \sigma < 1 - \sigma < 0$.

Thus it is sufficient to show that 
\[		|\zeta_{k(t)}(s)| = |\zeta_{\P^1_k}(s)| = \left| \dfrac{1}{ (1 - q^{-s})	(1 - q^{1-s}) } \right|	\leq	\frac{3}{\sigma - 1}	\]
Recall that
$|1 - q^{-s}| \geq |1 - |q^{-s}|| = 1 - q^{-\sigma} > 0$, so that
\[ 
|\zeta_{\P^1_k}(s)|	\leq  \dfrac{1}{ (1 - q^{-\sigma})	(1 - q^{1-\sigma}) }
\]

Therefore we want to show $\sigma - 1 \leq 3  (1 - q^{-\sigma})	(1 - q^{1-\sigma})$ for every $\sigma \in ]1,2[$, i.e.
\[ 3 (1-x)(1-qx) + \ln(x)/\ln(q)  + 1 \geq 0 \]
for every $x = q^{-\sigma} \in ]q^{-2}, q^{-1}[$. Since $\ln(q) > 0$, this is equivalent to show that 
\[
f_q(x) := 3 \ln(q) (1-x)(1-qx) + \ln(qx) \geq 0
\] 
is non-negative on $]q^{-2}, q^{-1}[$.
This follows from the three easy facts listed below:
\begin{itemize}
\item Firstly, $f_q(q^{-1})=0$ is clear. 

\item
Secondly, we have $f_q(q^{-2}) > 0.$ 
Indeed, since $q \geq 2$, we get
\begin{align*}
f_q(q^{-2}) 	&= 		\ln(q) \cdot (3 (1 - q^{-2}) (1 - q^{-1})	-   1)		\\
					&\geq 	\ln(q) \cdot (3 (1 - 2^{-2}) (1 - 2^{-1})	-   1)		\\
					&=		\ln(q) \cdot \left( 3 \cdot \frac{3}{8}  -   1 \right) > 0. 
\end{align*}

\item 
Thirdly, we check that $f'_q(q^{-1}) < 0$. Indeed, we have 
\[
f'_q(x) 		= 3 \ln(q) (2qx - q - 1) + \frac{1}{x},
\]
and using the inequality $q \geq 2$ again, we obtain		%and    3 \ln(q) - 1 > 0  for  q \geq 2  		and		e^2 < 8
\begin{align*}
f'_q(q^{-1})	&= 3 \ln(q) (1 - q) + q = (1 - q)(3 \ln(q) - 1) + 1 \\
					&\leq -(3 \ln(q) - 1) + 1 = 2 - 3 \ln(q)	\leq  2 - 3 \ln(2) < 0.
\end{align*}
\end{itemize}

Notice that $f'_q(x) = g(x) / x$, where $g$ is a convex quadratic function of $x$.
% if $a>b>0$ and $\phi(a) \geq 0 = \phi(b), \phi'(b) < 0,  x \phi'(x)$ convex on $[a,b]$ then $\phi \geq 0$ on $[a,b]$.
Let $a, b \in \R$ be the two zeros of $f'_q$ and note that we have $a < q^{-1} < b$ by the third observation $f'_q(q^{-1}) < 0$ above.
\begin{itemize}[---]
\item
If $f'_q(q^{-2}) \geq 0$, then $f_q$ is increasing on $]q^{-2}, a]$ and is decreasing on $[a, q^{-1}[$, so that $f_q(x) \geq 0$ holds true for every $x \in [q^{-2}, q^{-1}]$, since $f_q(q^{-2}) > 0 = f_q(q^{-1})$.

\item
If $f'_q(q^{-2}) < 0$, then $f_q$ is decreasing on $[q^{-2}, q^{-1}]$, so that $f_q$ is non-negative on this interval, because $f_q(q^{-2}) > 0 = f_q(q^{-1})$.
\end{itemize}

In all cases, we have $f_q \geq 0$ over $]q^{-2}, q^{-1}[$, as desired.
\end{proof}



Moreover, recall that if $E/K$ is non-constant, then by \cref{thm_L_function_is_polynomial}, we can write the $L$-function as a polynomial
\begin{equation}\label{L_function_as_polynomial}
\L(E / K, s) = \prod_{j=1}^{D_{E/K}} (1 - \beta_j q^{-s})
\end{equation}
for every $s \in \C$, where $|\beta_j| = q$ for every $j$.
On the other hand, the Euler product
\begin{equation}\label{Euler_product}
\L(E / K, s) = \prod_{v \in |C|} \prod_{j=1}^{2} (1 - a_{v, j} q_v^{-s})^{-1}
\end{equation}
(valid whenever $\re(s) > 3/2$), satisfies\footnote{
For all places $v$ of good reduction for $E$, we have an equality $|a_{v,j}| = q_v^{1/2}$.
}  
$|a_{v,j}| \leq q_v^{1/2}, q_v = q^{\deg(v)}$.

\Cref{L_function_as_polynomial,Euler_product} yield the upper bounds given respectively in lemmas \ref{Bound_factor_of_L_function} and \ref{Bound_L_function_by_zeta_K}.

\begin{lemma}\label{Bound_L_function_by_zeta_K}
If $\sigma = \re(s) > 3/2$, then
\[
| \L(E / K, s)| \leq \zeta_K(\sigma - 1/2)^{2d}
\quad\text{and}\quad
| \log(\L(E / K, s)) |	\leq	2d \; \log( \zeta_K(\sigma - 1/2) ).
\qedhere \]
\end{lemma}
\begin{proof}
The first inequality follows from the second one, so it is sufficient to prove the second inequality.
From (\ref{Euler_product}), we have
$\log( \L(E / K, s) ) = - \sum_v \sum_j \log( 1 - a_{v, j} q_v^{-s} ).$
Recall that if $|z| < 1$, then
\begin{equation}\label{inequality_log}
|\ln(1 - z)| = \abs{ - \sum_{n \geq 1} \dfrac{z^n}{n} } 
\leq \sum_{n \geq 1} \dfrac{ |z| ^n}{n} = - \ln( 1 - |z| ) = |\ln(1-|z|)|.
\end{equation}
Since $|a_{v,j} q_v^{-s}| \leq q_v^{1/2-\sigma} < 1$, we get
\begin{align*}
| \log( \L(E / K, s) ) | &\leq \sum_v \sum_j   - \log( 1 - |a_{v, j}| q_v^{-\sigma} ) 
 \\ & \leq 
\sum_v \sum_j   - \log(1 - q_v^{1/2-\sigma}) = \sum_v 2d \log((1 - q_v^{1/2-\sigma})^{-1}) 
\\ & = 2d \log( \zeta_K(\sigma - 1/2) ).		
\qedhere
\end{align*}
\end{proof}






\begin{lemma}\label{Bound_factor_of_L_function}
Let $s \in \C$ with $\sigma := \re(s) > 1$. Then
\[		| \log(\L(E/K, s)) | 	\leq	D_{E/K} |\log(1 - q^{1 - \sigma})|.		\qedhere \]
\end{lemma}
\begin{proof}
By \cref{L_function_as_polynomial,inequality_log}, we have
\begin{align*}
|\log \L(E / K, s)| &\leq \sum_{j=1}^{D} | \log( 1 - \beta_j q^{-s} )|	\\
&\leq 
\sum_{j=1}^{D} | \log( 1 - q^{1 - \sigma} ) | = D_{E / K} | \log( 1 - q^{1 - \sigma} ) |.		\qedhere
\end{align*}
\end{proof}






We can now prove lemma 7.2 from \cite{Hindry_Pacheco}.
\begin{proposition}\label{Lemma_7_2}
Set $\beta := 3 \cdot 2^{2g}$. Let $\rho \in ]0, 1 / \beta]$.
\begin{itemize}
\item If $\sigma := \re(s) \in [1    + \rho, 3/2]$, then 
\[
| \log(\L(E/K, s)) | 	\leq	2 D_{E/K} | \log(\rho) |
\]

\item If $\sigma := \re(s) \in [3/2 + \rho, 2[$, then
\[
| \log(\L(E/K, s)) | 	\leq 4 | \log(\rho) |.	\qedhere
\]
\end{itemize}
\end{proposition}
\begin{proof}
The first part readily follows from \cref{Bound_factor_of_L_function} and from the inequalities
\[ 
| \log(1  -  q^{1 - \sigma}) |	\leq	2 \log\left( \frac{1}{ \sigma - 1 } \right)	\leq		2 \log\left( \frac{1}{ \rho } \right)		= 2 |\log(\rho)|.
\]
the first one being valid if $q \geq 2$ and $\sigma \in ]1, \frac{3}{2}]$.

The second part follows from \cref{Bound_L_function_by_zeta_K,upper_bound_on_zeta}:
if $ \sigma \in ]3/2 + \rho, 2[$ then
$$| \log(\L(E/K, s)) |	\leq	2 \, \log( \zeta_K(\sigma - 1/2) )	\leq  2 \log\left( \dfrac{3 \cdot 2^{2g}}{ \rho } \right) = 2 ( \log(3 \cdot 2^{2g})  +   |\log(\rho)|) \leq 4 |\log(\rho)|.$$
\end{proof}

We now deduce lemma 7.3 from \cite{Hindry_Pacheco}.

\begin{proposition}\label{Lemma_7_3}
Set $\beta := 3 \cdot 2^{2g}$.
Assume that $D_{E/K} \geq 9$ and that $\rho := \dfrac{1}{2} \dfrac{ \log \log D_{E/K} }{ \log D_{E/K} } \leq 1/\beta$.
Define $\gamma := 2 \rho$.

Then for every $t \in \R$, we have
\[ | \log(\L(E/K, 1 + \gamma + it)) |		\leq	12  D_{E/K}  \dfrac{  \log \log (D_{E/K})  }{\log D_{E/K}}.	\qedhere	\]
\end{proposition}
\begin{proof}
Consider the strip $S := \{ s \in \C \tq 1 + \rho \leq \re(s) \leq \frac{3}{2} + \rho \}$. 
We apply Phragmen–Lindelöf theorem to\footnote{%
Notice that when $s =  1 + \gamma + it$ then $|D_{E/K}^{ 1 - 2(s-1-\rho) }| = D_{E/K}^{1 - 2(\gamma - \rho)}$. %since D^{i t} has modulus 1.
}  
$\frac{\log \L(E/K, s)}{ D_{E/K}^{1 - 2(s-1-\rho)} }$ over this strip, where we know the bounds on the boundaries thanks to \cref{Lemma_7_2}. %https://en.wikipedia.org/wiki/Lindel%C3%B6f%27s_theorem
Then for every $\rho \in ]0, 1/\beta],  \gamma \in [\rho, \rho + \frac{1}{2}]$ and every $t \in \R$, one has
\begin{equation}\label{upper_bound_L_function_Lindelof_Phragmen_1}
| \log(\L(E/K, 1 + \gamma + it)) |		\leq	4   D_{E/K}^{ 1 - 2(\gamma - \rho) }   |\log(\rho)|.
\end{equation}



Define $\rho := \dfrac{1}{2} \dfrac{ \log \log D_{E/K} }{ \log D_{E/K} } \in ]0, 1/2]$, and set $\gamma := 2\rho \in [\rho, \rho + \frac{1}{2}]$.
Since we assume that $\rho \leq 1/\beta$, the upper bound \eqref{upper_bound_L_function_Lindelof_Phragmen_1} yields
\begin{align*}
| \log(\L(E/K, 1 + \gamma + it)) |		&\leq	4   D_{E/K}^{ 1 - 2\rho }   |\log(\rho)|
\\ & = 4   D_{E/K} \cdot D_{E/K}^{  -\frac{ \log \log D_{E/K} }{\log D_{E/K}}  }  \left| \log\left( \dfrac{ \log \log D_{E/K} }{ 2 \log D_{E/K} }  \right) \right|
\\ & = 4   \dfrac{ D_{E/K} }{\log D_{E/K}}	 | \log \log \log(D_{E/K})     -     \log \log (D_{E/K})   -   \log(2)    |
\\ & \leq 4   \dfrac{ 3 D_{E/K} }{\log D_{E/K}}	 \log \log (D_{E/K}) ,
\end{align*}
where the last inequality holds if $D_{E/K} \geq 9$.
\end{proof}




We now use the functional equation for $\L(E/K, s)$ to get an upper bound on the vertical line $\re(s) = 1 - \rho$, and then another application of Phragmen–Lindelöf principle will imply the following result.
\begin{proposition}\label{Lemma_7_4}
There exists a constant $D_0>0$ (depending on $g$ but not on $q$) such that whenever $D_{E/K} \geq D_0$, we have %D_0 > 9 taken so that log log D / (2 log D) ≤ 1/beta.			I am not sure anymore why I need 13 and not 12 in the upper bound...
\[
|\L(E/K, s)|	\leq \exp\left( D_{E/K} \dfrac{  \log \log (D_{E/K})  }{\log D_{E/K}} (13 + \log(q)) \right),
\]
for every $s \in S'$ where $S' := \{s \in \C  \tq  1 - \gamma \leq \re(s) \leq 1+\gamma\}$ and $\gamma := \dfrac{  \log \log (D_{E/K})  }{\log D_{E/K}}$.
\end{proposition}
\begin{proof}
The functional equation of $\L(E/K, s)$ is $\L(E/K, 2-s) = \pm q^{(s-1) D_{E/K}}  \L(E/K, s)$ by \cref{thm_L_function_is_polynomial}.
In particular,
\[
\L(E/K, 1 - \gamma - it) = \L(E/K, 2 - (1+\gamma+it)) = \pm  q^{(\gamma+it) D_{E/K}}  \L(E/K, 1+\gamma+it)
\] %(-1)^{r_{\an}}
Using \cref{Lemma_7_3}, we deduce that when $D_{E/K}$ is large enough ($D_{E/K} \to \infty$), we have the inequality
\[
| \log \L(E/K, 1-\gamma - it) | 	\leq	D_{E/K} \dfrac{  \log \log (D_{E/K})  }{\log D_{E/K}} (13 + \log(q)) 
\] 
for every $t \in \R$, where $\gamma := \dfrac{  \log \log (D_{E/K})  }{\log D_{E/K}}$.

Combining this upper bound with the one obtained in \cref{Lemma_7_3}, we deduce from Phragmen–Lindelöf principle that
the above inequality holds true whenever $1-\gamma - it$ is replaced by $s \in S'$. This terminates the proof.
\end{proof}




We can now derive an upper bound for the derivatives of $\L(E/K, s)$ at $s=1$ using Cauchy integral formulas, and Brumer's bound from \cref{thm_Brumer_bound}. This corresponds to theorem 7.5 in \cite{Hindry_Pacheco}.
\begin{proposition}\label{Bound_on_derivative}
There exists a constant $c_1 > 0$ (depending on $g$ and the constant $c_0$ given in the statement of \cref{thm_upper_bound_BS_Hindry_Pacheco}, but does not depend on $q$) %coming from Brumer's bound
such that
\begin{align*}
\log |\L^*(E/K)|		&\leq		f_{E/K} \dfrac{  \log \log (f_{E/K})  }{\log f_{E/K}} (13 + c_1 \log(q))	. \qedhere
\end{align*}
\end{proposition}
\begin{proof}
Let $C$ be the circle of center $1$ and radius $\gamma$ as in \cref{Lemma_7_4}, and $r$ be the analytic rank of $E/K$. Then Cauchy integral formula
\[
\dfrac{ \L^{(r)}(E/K, 1) }{ r! }	= \dfrac{1}{2 \pi i} \int_C \dfrac{\L(E/K, z)}{(z-1)^{r+1}} \dd z
\]
yields % For the 1/log(q)^r, See Ulmer 2019, p. 1073   ...   but Hindry-Pacheco don't put any 1/log(q)^r cf p. 6 !!! – the factor 1/log(q)^r annoying if q = 2 since log(q) < 1.   We don't put it, as Reg \in \Q \cdot \log(q)^r (See p. 9 \cite{Hindry_Pacheco}, or \cite{Gross_lectures_BSD} p. 10) –    cf Remarque 1.2.4 and 1.2.8 in \cite{Griffon_PhD}, and Remark 2.5. in \cite{Hindry_Pacheco}.
\begin{align*} 
|\L^*(E/K)|	
&=
\left| \dfrac{ \L^{(r)}(E/K, 1) }{ r! } \right| \leq \dfrac{1}{2 \pi} \mathrm{length}(C) \cdot \gamma^{-(r+1)} \cdot \max_{z \in C} |\L(E/K, z)|
\\&= 
\gamma^{-r}		\cdot 	\max_{z \in C} |\L(E/K, z)|.
\end{align*}

By Brumer's \cref{thm_Brumer_bound} and since we assume that $q \leq f_{E/K}^{c_0}$, we have 
\begin{align}
r 
&\leq
\dfrac{ f_{E / K} + 4g_C - 4 }{ 2\log(f_{E / K}) } \log(q)	+	
					\dfrac{ 		c_0 f_{E / K} \log(q) \log(f_{E/K}) 		}{ 			 \log(f_{E / K})^2 (1 - 2^{-1/2})^2 		}
					+	1 + 2 \beta_K + \dfrac{c_K c_0 \log(f_{E/K}) }{2 \log(f_{E / K}) }.
\notag	\\&\leq
\dfrac{ f_{E / K} + 4g_C - 4 }{ 2\log(f_{E / K}) } \log(q)	+	
					\dfrac{ 		c_0 \cdot f_{E / K} \log(q) 		}{ 	 \log(f_{E / K}) (1 - 2^{-1/2})^2 		}
					+	\O_{c_0, g}(1)
\notag	\\&\leq
c'_1 \cdot  \dfrac{ f_{E / K}  \log(q) }{ \log(f_{E / K}) }
\label{eq_rank_upper_bound}
\end{align}
for some $c'_1 > 0$ which depends on $g, c_0$ but not on $q$.


Since $C$ is contained in the strip $S'$, \cref{Lemma_7_4} implies that if $f_{E/K}$ is large enough, then	%log log D \geq 1
\begin{align*}
|\L^*(E/K)|		&\leq		\left( \dfrac{  \log (D_{E/K})  }{ \log \log D_{E/K}} \right)^r \exp\left( D_{E/K} \dfrac{  \log \log (D_{E/K})  }{\log D_{E/K}} (13 + \log(q)) \right)
\\ & \leq 
\exp\left(	c'_1 \cdot \dfrac{f_{E/K} \log(q)}{ \log(f_{E/K}) }   \log \log D_{E/K} 	\;+\;
						D_{E/K} \dfrac{  \log \log (D_{E/K})  }{\log D_{E/K}} (13 + \log(q))		 \right).
\end{align*}
Thus, since $D_{E/K} \sim f_{E/K}$ (by \cref{thm_L_function_is_polynomial}, as $g$ is fixed), we get
\begin{align*}
\log |\L^*(E/K)|		&\leq		f_{E/K} \dfrac{  \log \log (f_{E/K})  }{\log f_{E/K}} (13 + c_1 \log(q)),
\end{align*}
where $c_1 := 1 + c'_1$. This concludes the proof.
\end{proof}







Finally, we are able and ready to prove the upper bound on the Brauer–Siegel ratio, as stated in \cref{thm_upper_bound_BS_Hindry_Pacheco}.

\begin{proof}[Proof of \cref{thm_upper_bound_BS_Hindry_Pacheco}]
By \cref{thm_BSD_true_for_isotrivial}, all elliptic curves $E / K$ in the statement satisfy the \hyperref[item_BSD_formula]{BSD formula}.

Fix $\epsilon > 0$.
On the one hand, we have $|E(K)_{\tors}|^2  \ll \O(1)$ by \cref{prop_uniform_bound_torsion}, where the implicit constant depends only on $g$ (note that we assumed that $f(E/K)^{c_0} \geq q = |k|$, so this forces $E / K$ to be non-constant).
On the other hand, $c_v(E/K) \geq 1$ for every place $v$, so BSD formula leads to
\[  		 |\Sha(E/K)|  \cdot  \Reg(E/K)    \leq   L^*(E/K)  \cdot q^{g-1} \cdot  H(E/K). \]
As mentioned in \cref{rmk_L_functions}, we have $L^*(E/K) = \log(q)^{-r} \L^*(E/K)$ where $r$ is the rank of $E/K$. When $q > 2$, we have $L^*(E/K) \leq \L^*(E/K)$ since $\log(q) > 1$. When $q = 2$, we have $\log(q)^{-1} > 1$ so \cref{eq_rank_upper_bound} yields
\begin{align*}
(\log(q)^{-1})^r 
&= 
\exp[ 		r \cdot (- \log \log(q)) 		]
\leq 
\exp\Big( c'_1  \cdot  \dfrac{ f_{E / K}  \log(2) }{ \log(f_{E / K}) }   \cdot (- \log \log(2) )	 \Big)
\end{align*}
Let us set $c'_2 := c'_1 \cdot \frac{\log(2)}{- \log \log(2)} > 0$ and $c_2 := \max\set{c'_2, 1}$ (which does not depend on $q$). Thus in all cases we have $(\log(q)^{-1})^r  \leq \exp\big( c_2  \cdot  \frac{ f_{E / K} }{ \log(f_{E / K}) }  \big)$.



Since $f_{E/K} \leq \discr{E/K}$ by \cref{thm_Ogg_Pesenti_Szpiro}, we get 
\[
f_{E/K} \cdot  \dfrac{ \log \log f_{E/K} }{ \log f_{E/K} } \ll_{\epsilon}  \epsilon \cdot \discr{E/K},
\]
as $f_{E/K} \to +\infty$.
%
Thereby, \cref{Bound_on_derivative} gives, when $f_{E/K} \to \infty$:
\begin{align*}
|\Sha(E/K)|  \cdot  \Reg(E/K)     
&\ll_g
H(E/K) 	\cdot		q^{g-1} \cdot 	 \log(q)^{-r} \cdot 
\\ & \qquad \cdot 
\exp\!\left( 	f_{E/K} \dfrac{  \log \log (f_{E/K})  }{\log f_{E/K}} (13 + c_1 \log(q)) 		\!\right)
%
\\ & \ll_g
%
H(E/K)^{1 + \epsilon} \cdot 	 \exp\Big( c_2  \cdot  \frac{ f_{E / K} }{ \log(f_{E / K}) }  \Big) \cdot 
\\ & \hspace{2.6cm} \cdot
\exp\!\left( 	f_{E/K} \dfrac{  \log \log (f_{E/K})  }{\log f_{E/K}} (13 + c_1 \log(q)) 		\!\right)
%
\\ & \leq
%
H(E/K)^{1 + \epsilon} 								\cdot 	
\exp(c_2 \cdot \epsilon \cdot f_{E/K})		\cdot
\\& \hspace{2.6cm} \cdot
\exp\left[ 	\epsilon \cdot	 \discr{E/K}   (13 + c_1 \log(q)) 		\right]
%
\\ & \leq
%
H(E/K)^{1 + \epsilon} \cdot  H(E/K)^{ \frac{12 c_2}{\log(q)} \epsilon } \cdot 	
H(E/K)^{ \frac{1}{ \log(q) } \cdot \epsilon \cdot (13 + c_1 \log(q)) }
\\ & \leq
%
H(E/K)  \cdot 	H(E/K)^{ \epsilon \cdot (1 + 12 c_2 \log(2)^{-1} + 13 \log(2)^{-1} + c_1) }
\end{align*}
The third inequality comes from the fact that $\frac{\log \log(t)}{ \log(t) } \to 0$ and $\frac{1}{\log(t)}$ when $t \to +\infty$. 		
%and use $e = q^{ 1 / \log(q) }$		and		f_{E/K} \leq \discr{E/K}.

In other words, we proved that for every $\epsilon > 0$, there are some $B_{\epsilon, g, c_0} , B'_{\epsilon, g, c_0} > 0$ such that $f_{E/K} \geq B_{\epsilon, g, c_0}$ implies %B' coming from the \ll_g above, and c_2(g, c_0).
\[
|\Sha(E/K)|  \cdot  \Reg(E/K)   \leq  B'_{\epsilon, g, c_0} \cdot  H(E/K)^{1+\epsilon},
\]
for every $E/K$ as in the statement of the theorem, which finishes the proof.
\end{proof}