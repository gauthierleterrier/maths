"""
This is the file "L_functions_y2_x3_1_tm.sage". It can be used to study the L-function, analytic rank and geometric rank of the curves y^2 = x^3 + b + b' t^m over F_q(T) where q is a power of a prime p = 1 mod 3. See chapter 4, especially §4.2 in the thesis.
"""

CycloT.<T> = PolynomialRing(UniversalCyclotomicField())


def u(q, d, r):
    """
    INPUT
        - A prime power q 
        - An integer d ≥ 1 coprime to q
        - An element r in {1, ..., d} (or in Z / d Z)

    OUTPUT
        - The multiplicative order of q modulo d / gcd(d, r), as in definition 1.4.15
    """
    D = d // gcd(d, r)
    return mod(q, D).multiplicative_order()
#Note: we will need to compute Jacobi sums over fields of order Q^(u(Q, m, r)) and for given Q, m, this attains its maximum at r=1


def Repres_Orbits(m, Q, set_X):
    """
    INPUT
        - An integer m ≥ 1
        - A prime power Q (typically such that our elliptic curve is defined over F_Q(t)
        - A subset "set_X" of {0, ..., m - 1} (or of Z / m Z)

    OUTPUT
        - List of representatives for the quotient of set_X by the powers of Q acting on Z / m Z
    """
    Repres = []
    Union_orbits = []
    orderQ = mod(Q, m).multiplicative_order()
    for x in set_X:
        if x not in Union_orbits:
            Union_orbits += list({mod(Q, m)^j * x for j in range(orderQ)})
            Repres += [x]
    return Repres





###################################################################
# Characters and Jacobi sums
###################################################################

def Legendre(Q, x):
    """
    INPUT
        - A power Q of an odd prime
        - An element x of GF(Q)

    OUTPUT
        - The value of Legendre symbol of GF(Q) at x
    """
    if x==0: return 0
    value = GF(Q)(x^( (Q-1) // 2))
    if value == -1: return -1
    else:           return  1



def Leg(Q):
    """
    INPUT
        - A power Q of an odd prime

    OUTPUT
        - The Legendre symbol of F_Q, returned as a character (a function from F_Q to {-1, 0, 1})
    """
    def legendre(x):
        return Legendre(Q, x)
    return legendre



def theta(Q, n, d, r):
    """
    INPUT
        - A prime power Q
        - An integer d ≥ 1
        - An element r in {1, ..., d}
        - An integer n ≥ 1 which is a multiple of u(q, d, r) as defined above

    OUTPUT
        - Returns the multiplicative character on F_{Q^n}, introduced in Definition 1.4.15 , of order d / gcd(d, r)
    (see also Remark 1.4.18).
    """
    ur = u(Q, d, r)
    assert(n % ur == 0)
    def my_theta(x):
        if x == 0:
            if r == 0: return 1
            else:      return 0
        kn = GF(Q^n)
        g = kn.multiplicative_generator()
        e = discrete_log(x, g, Q^n - 1, operation='*')
        Theta_of_x = E(Q^n - 1)^e #= exp(2*pi*I*e / (Q^n - 1)) #Teichmuller char at x, for x \in GF(Q^n).
        #E(...) : root of unity in a cyclotomic field
        return Theta_of_x^( (Q^n - 1)*r // d )
    return my_theta


def psi3(Q):
    """
    INPUT
        - A prime power Q such that Q = 1 mod 3

    OUTPUT
        - The character psi of order 3 over F_Q^* as introduced in Definition4.1.1 (see also remark 1.4.18).
    """
    assert( Q % 3 == 1 )
    g = GF(Q).multiplicative_generator()
    def my_psi(x):
        if x==0: return 0
        e = discrete_log(x, g, Q - 1, operation='*')
        return E(3)^e #3 divides Q-1
    return my_psi



def psi3_inv(Q):
    """
    INPUT
        - A prime power Q such that Q = 1 mod 3

    OUTPUT
        - The inverse of the character psi of order 3 over F_Q^* as introduced in Definition4.1.1 (see also remark 1.4.18).
    """
    def my_psi(x):
        if x == 0: return 0
        return psi3(Q)(x)^(-1)
    return my_psi




def psi6_(q, x, g, g1):
    """
    INPUT
        - A prime power q such that q = 1 mod 6
        - An element x in GF(q)
        - g = GF(q).multiplicative_generator() [pre-computed]
        - g1 = g^((q-1) // 6)                  [pre-computed]

    OUTPUT
        The value (in overline(Q)) at the element x of the character over F_q of order 6 equal to psi*lambda (using the notation from definition 4.1.1)
    """
    if x == 0: return 0
    y  = x^((q-1) // 6)
    for e in range(6):
        if y == g1^e:
            return E(6)^e



def Jacobi_psi3_Leg_chi(q, r, m, inverse=1):
    """
    INPUT
        - A prime power q such that q = 1 mod 3
        - an integer m ≥ 1 coprime to q
        - an integer r in {1, ..., m-1} which is not equal to m/6 if m is divisible by 6
        such that q = 1 mod m/gcd(r, m)
        [for instance q = Q^u, for some prime Q = 1 mod 3 and u = u(Q, m, r)]
        - An integer "inverse" which is +1 or -1 (by default, it is +1)


    OUTPUT
        A pair (jacobi_1 , jacobi_2) where
            - jacobi_1 is the Jacobi sum J(psi, lambda) where psi is the character of order 3 on F_q as in Definition 4.1.1 [see also Remark 1.4.18] and lambda is the Legendre symbol.
            - jacobi_2 is the Jacobi sum J(psi * lambda, theta_{F_q, m, r})
        The product jacobi_1 * jacobi_2 is the triple Jacobi sum J_{F_q, m, r} introduced in equation 4.2.6, appearing in the L-function of E'_{m, b, b'} (see theorem 4.1.2 and equation 4.1.5)

        If "inverse = -1" then the ouput is the same as above but replacing psi by psi^(-1)


    REMARK
        See the file "q_7__table_Jacobi_sums__explanations.ipynb" for some explanations.
    """

    g = GF(q).multiplicative_generator()
    g1 = g^((q-1) // 6)
    x = g
    jacobi_1 = 0
    jacobi_2 = 0
    zeta_m = E(m)^r

    for e in range(1, q - 1):
        e1 = e % (3*m)
        jacobi_1 += (E(3)^(e1*inverse) * Legendre(q, 1 - x))          #It typically depends on r and m, as we take q = Q^(u(Q, m, r)) in practice.
        jacobi_2 += (zeta_m^e1         * psi6_(q, 1 - x, g, g1)^(-inverse)) #lambda * psi3 = psi6^(-1), where psi3 = psi6^2. Indeed we have lambda(g) * psi3(g) = -1 * exp(2πi/3) = exp(πi(1 + 2/3)) = exp(2πi * 5/6) = E(6)^(-1)
        x    *= g
    return jacobi_1 , jacobi_2


def list_jacobi_sums(m, Q):
    """
    INPUT
        - A prime power Q such that Q = 1 mod 3
        - An integer m ≥ 1 coprime to Q

    OUTPUT
        - A list of elements of the form [r, (jac1, jac2)] where:
            —— r ranges from 1 to m-1
            —— jac1 is the Jacobi sum J(psi, lambda) where psi is the character of order 3 on F_q as in Definition 4.1.1 [see also Remark 1.4.18] and lambda is the Legendre symbol.
            —— jac2 is the Jacobi sum J(psi * lambda, theta_{F_Q, m, r})
               The product jac1 * jac2 is the triple Jacobi sum J_{F_Q, m, r} introduced in equation 4.2.6, appearing in the L-function of E'_{m, b, b'} (see theorem 4.1.2 and equation 4.1.5)
    """
    L = []
    for r in range(1, m):#We could speed this up by only considering a set of representatives for the action of power of Q modulo m
        ur = u(Q, m, r)
        L.append([r, Jacobi_psi3_Leg_chi(Q^ur, r, m)])
    return L




###################################################################
# L-functions and analytic rank
###################################################################

def LFunction(m, a, b, Q, verbose=False):
    """
    INPUT
        - A prime power Q coprime to 6
        - An integer m ≥ 1 coprime to Q
        - Two elements a, b in F_Q^*
        - A boolean "verbose" (if True, some data is printed)

    OUTPUT
        - The L-function of the elliptic curve E'_{m, a, b} : y^2 = x^3 + a + b*t^m over F_Q(t), as a polynomial in Z[T]
        - The list of factors of the L-function appearing in the product in theorem 4.1.2 (they are polynomials in overline(Q)[T])
    """
    L = 1
    set_plus  = [mod(r, m) for r in range(1, m) if m % 6 != 0 or (m % 6 == 0 and r !=   m//6)]
    set_minus = [mod(r, m) for r in range(1, m) if m % 6 != 0 or (m % 6 == 0 and r != 5*m//6)]
    list_fact = []

    if( Q % 3 == 1 ):
        for r in range(1, m):
            r0 = mod(r, m)
            ur = u(Q, m, r)
            Qur = Q^ur
            chi_r = theta(Q, ur, m, r)
            if verbose: print("ur =", ur)

            if r0 in Repres_Orbits(m, Q, set_plus):
                alpha_plus  = prod(Jacobi_psi3_Leg_chi(Qur, r, m))             * Leg(Qur)(a) * chi_r(-a/b) * psi3(Qur)(-a)
                L *= (1 - alpha_plus * T^(ur))
                list_fact.append(1 - alpha_plus * T^(ur))
                if verbose: print(alpha_plus)

            if r0 in Repres_Orbits(m, Q, set_minus):
                alpha_minus = prod(Jacobi_psi3_Leg_chi(Qur, r, m, inverse=-1)) * Leg(Qur)(a) * chi_r(-a/b) * psi3_inv(Qur)(-a)
                L *= (1 - alpha_minus * T^(ur))
                list_fact.append(1 - alpha_minus * T^(ur))
                if verbose: print(alpha_minus)

        return L , list_fact

    if( Q % 3 == 2 ):
        for r in range(1, m):
            r0 = mod(r, m)
            ur = u(Q^2, m, r)
            Qur = Q^(2*ur)
            chi_r = theta(Q^2, ur, m, r)
            if verbose: print("ur =", ur)

            if r0 in Repres_Orbits(m, Q^2, set_plus):
                alpha_plus  = prod(Jacobi_psi3_Leg_chi(Qur, r, m))             * Leg(Qur)(a) * chi_r(-a/b) * psi3(Qur)(-a)
                L *= (1 - alpha_plus * T^(2*ur))
                list_fact.append(1 - alpha_plus * T^(2*ur))
                if verbose: print(alpha_plus)

        return L , list_fact




def analytic_rank(m, a, b, Q, verbose=False):
    #Note: this could have been combined with the function "Lfunction" above.
    """
    INPUT
        - A prime power Q coprime to 6
        - An integer m ≥ 1 coprime to Q
        - Two elements a, b in F_Q^*
        - A boolean "verbose" (if True, some data is printed)

    OUTPUT
        - The analytic rank of the elliptic curve E'_{m, a, b} : y^2 = x^3 + a + b*t^m over F_Q(t)
    """

    rank = 0
    set_plus  = [mod(r, m) for r in range(1, m) if m % 6 != 0 or (m % 6 == 0 and r !=   m//6)]
    if( Q % 3 == 1 ):
        Repres_Orbits_plus  = Repres_Orbits(m, Q, set_plus)
        for r0 in Repres_Orbits_plus:
            r = ZZ(r0)
            ur = u(Q, m, r)
            chi_r = theta(Q, ur, m, r)
            Qur = Q^ur
            alpha_plus  = prod(Jacobi_psi3_Leg_chi(Qur, r, m)) * Leg(Qur)(a) * chi_r(-a/b) * psi3(Qur)(-a)
            if verbose: print("ur =", ur, "\t alpha_plus =", alpha_plus)
            if alpha_plus == Qur: rank += 1
        return 2 * rank #see equation 4.2.14

    if( Q % 3 == 2 ):
        Repres_Orbits_plus  = Repres_Orbits(m, Q^2, set_plus)
        for r0 in Repres_Orbits_plus:
            r = ZZ(r0)
            ur = u(Q^2, m, r)
            chi_r = theta(Q^2, ur, m, r)
            Qur = Q^(2*ur)
            alpha_plus  = prod(Jacobi_psi3_Leg_chi(Qur, r, m)) * Leg(Qur)(a) * chi_r(-a/b) * psi3(Qur)(-a)
            if verbose: print("ur =", ur, "\t alpha_plus =", alpha_plus)
            if alpha_plus == Qur: rank += 1
        return rank



def analytic_rank_given_Jacobi_sums(m, a, b, Q, list_jacobi, verbose=False):
    """
    INPUT
        - A prime power Q coprime to 6
        - An integer m ≥ 1 coprime to Q
        - Two elements a, b in F_Q^*
        - A list "list_jacobi" which is the output of "list_jacobi_sums(m, Q)" defined above. The idea is to have pre-computed this list, so that the computation takes much less time than using "analytic_rank(m, a, b, Q)" from above.
        - A boolean "verbose" (if True, some data is printed)

    OUTPUT
        - The analytic rank of the elliptic curve E'_{m, a, b} : y^2 = x^3 + a + b*t^m over F_Q(t)
    """
    if( Q % 3 == 1 ):
        if list_jacobi == 0:
            if gcd(m, 2*3*5) == 1:
                return 0 #See Corollary 4.2.23.2
            return '?'

        number_of_alpha_plus_equal_to_positive_integer = 0

        set_plus  = [mod(r, m) for r in range(1, m) if m % 6 != 0 or (m % 6 == 0 and r !=   m//6)]
        Repres_Orbits_plus  = Repres_Orbits(m, Q, set_plus)

        for r, (jac1, jac2) in list_jacobi:
            ur = u(Q, m, r)
            if verbose: print("\n r, ur =", r, ur)
            chi_r = theta(Q, ur, m, r)
            Qur = Q^ur
            alpha_plus  = jac1 * jac2 * Legendre(Qur, a) * chi_r(-a/b) * psi3(Qur)(-a)
            if verbose: print("alpha_plus:", alpha_plus)
            if mod(r, m) in Repres_Orbits_plus and alpha_plus == Qur:
                number_of_alpha_plus_equal_to_positive_integer += 1

        return 2 * number_of_alpha_plus_equal_to_positive_integer #We use equation 4.2.14


    if(Q % 3 == 2):
        if list_jacobi == 0:
            return '?'
        number_of_alpha_plus_equal_to_positive_integer = 0

        set_plus  = [mod(r, m) for r in range(1, m) if m % 6 != 0 or (m % 6 == 0 and r !=   m//6)]
        Repres_Orbits_plus  = Repres_Orbits(m, Q^2, set_plus)

        for r, (jac1, jac2) in list_jacobi:
            ur = u(Q^2, m, r)
            if verbose: print("\n r, ur =", r, ur)
            chi_r = theta(Q^2, ur, m, r)
            Qur = Q^(2*ur)
            alpha_plus  = jac1 * jac2 * Legendre(Qur, a) * chi_r(-a/b) * psi3(Qur)(-a)
            if verbose: print("alpha_plus:", alpha_plus)
            if mod(r, m) in Repres_Orbits_plus and alpha_plus == Qur:
                number_of_alpha_plus_equal_to_positive_integer += 1

        return number_of_alpha_plus_equal_to_positive_integer




###################################################################
# Bounds on rank and geometric rank
###################################################################

"""
We define the sets S and M from Definition 4.2.14
"""

set_S = [1/2, 1/3, 2/3, 1/4, 3/4, 1/5, 2/5, 3/5, 4/5, 5/6, 2/9, 5/9, 8/9, 5/12, 11/12, 5/18, 11/18, 17/18, 5/24, 11/24, 17/24, 23/24, 11/30, 17/30, 23/30, 29/30, 11/60, 17/60, 23/60, 29/60, 41/60, 47/60, 53/60, 59/60]

set_M = [2, 3, 4, 5, 6, 9, 12, 18, 24, 30, 60]



def frac(x):
    """Given a real number x, returns the fractional part of x, in [0, 1["""
    return x - floor(x)



def upper_bound_rank(m, q):
    """
    INPUT
        - A power q of a prime p = 1 mod 3
        - An integer m > 0 coprime to p

    OUTPUT
        An upper bound on the rank of E'_{m, b, b'} : y^2 = x^3 + b + b' t^m over F_q(t), valid for every b, b' in F_q^*
    """
    X_m = srange(1, m)
    if m % 6 == 0:
        X_m.remove(m // 6)
    R_m = Repres_Orbits(m, q, X_m)
    return 2 * len([r for r in R_m if frac(r/m) in set_S])





def geometric_rank(m):
    """
    Given an integer m > 0, this returns the geometric rank of y^2 = x^3 + b + b' t^m over overline(F_p)(t), for any prime p = 1 mod 3 and any b,b' in F_p^*. This is also the geometric rank over k(t) where k is any algebraically closed field of characteristic 0. See Corollary 4.2.23.
    """
    X_m = srange(1, m)
    if m % 6 == 0:
        X_m.remove(m // 6)
    return 2 * len([r for r in X_m if frac(r/m) in set_S])




###################################################################
# Purity
###################################################################

def power_integer(z, bound=2160):
    """
    INPUT
        - A complex number z
        - A positive integer "bound" (default: 2160, value from theorem 4.2.1)

    OUTPUT
        - An integer n between 1 and "bound" such that z1^n is an integer, if such an n exists; return 0 otherwise
    """
    z1 = 1
    for n in range(1, bound + 1):
        z1 *= z
        if z1 in QQ and floor(z1) == z1:
            return n
    return 0




def is_it_pure(z, bound=2160, naive=False):
    """
    INPUT
        - A complex number z
        - A positive integer "bound" (default: 2160, value from theorem 4.2.1)
        - A boolean "naive" (to choose which method to use)

    OUTPUT
        - A boolean which is True iff there is an integer d between 1 and "bound" such that z^d is a rational number (Note: when z is triple Jacobi sum, the modulus is an integer, so that z^d belongs to Q iff z^d belongs to R iff z^d belongs to Z, for any d > 0)
    """
    if not naive:
        z1 = z
        for d in range(1, bound + 1):
            z1 = z1^d
            if z1 in QQ:
                return True
        return False
    if naive:
        for d in range(1, bound + 1):
            if z^d in QQ:
                return True
        return False



def degree_pure(z, bound=2160):
    """
    INPUT
        - A complex number z
        - A positive integer "bound" (default: 2160, value from theorem 4.2.1)
        - A boolean "naive" (to choose which method to use)

    OUTPUT
        - The smallest integer d between 1 and "bound" such that z^d is a rational number (Note: when z is triple Jacobi sum, the modulus is an integer, so that z^d belongs to Q iff z^d belongs to R iff z^d belongs to Z, for any d > 0)
    """
    for d in range(1, bound + 1):
        if z^d in QQ:
            return d
    return 0


def degree_positive(z, bound=2160):
    """
    INPUT
        - A complex number z
        - A positive integer "bound" (default: 2160, value from theorem 4.2.1)
        - A boolean "naive" (to choose which method to use)

    OUTPUT
        - The smallest integer d between 1 and "bound" such that z^d is a POSITIVE rational number (Note: when z is triple Jacobi sum, the modulus is an integer, so that z^d belongs to Q iff z^d belongs to R iff z^d belongs to Z, for any d > 0)
    """
    for d in range(1, bound + 1):
        zd = z^d
        if zd in QQ and zd > 0:
            return d
    return 0



def positive_Jacobi_sums(LIST, p, m, b1, b2):
    """
    INPUT
        - A prime p = 1 mod 3
        - An integer m coprime to p
        - b1, b2 : elements of F_p^*
        - A list "LIST" whose elements are of the form [r, (jac1, jac2)], as given in files like "q_19__table_Jacobi_sums.sage". Here r runs in Z/mZ and Jac1 = J(psi, lambda)$, Jac2 = J(psi lambda, theta_{k_{u(r)}, m, r})$ are as in theorem 4.2.1


    OUTPUT
        - The smallest integer N ≥ 1 such that the geometric rank of E'_{m, b_1, b_2} is attained over F_{p^N}(t)
        - The geometric rank is also returned
    """
    list_r = []
    for r, (jac1, jac2) in LIST:
        if r/m in set_S:#  equivalent to "is_it_pure(jac1*jac2, bound=2161, naive=False) == True" by Proposition 4.2.15
            ur      = u(p, m, r)
            Q       = p^ur
            psi_3   = psi3(Q)
            theta_r = theta(Q, ur, m, r)
            alpha   = jac1 * jac2 * Legendre(Q, b1) * psi_3(-b1) * theta_r(-b1 * b2^(-1))
            list_r.append([r, degree_positive(alpha)])
    #The geometric rank is then equal to 2*len(list_r) ; the   "2*"  comes from equation 4.2.14 (we have a product over epsilon = ± 1 in the L-function).

    #We use now equation 4.2.1 to determine N.
    N = 1
    while True:
        if all( [N / gcd(N, u(p, m, r)) % deg == 0   for   [r, deg] in list_r] ):
            return N, 2*len(list_r)
        N += 1





def L_function_extension(list_factors, Q, deg_extension):
    """
    INPUT
        - The list of factors of an L-function L(E / F_Q(t), T) as the second output by the function "LFunction" above
        - A prime power Q
        - An integer deg_extension ≥ 1

    OUTPUT
        - The L-function of E over F_{Q^n}(t) where n = deg_extension (See Lemma 4.2.9)
        - The analytic rank of E over F_{Q^n}(t)

    EXAMPLE OF USE:
        L, factors = LFunction(m, a, b, Q)
        for N in range(1, 10):
        _, rank    = L_function_extension(factors, Q, N)
        print(N, "\t", rank)
    """
    N = deg_extension
    list_factors_extension = []
    rank = 0
    for f in list_factors:
        ur    = f.degree()
        alpha = (-f).coefficients(sparse=False)[-1]#leading coefficient
        g     = gcd(N, ur)

        list_factors_extension.append(    (1 - alpha^(N // g) * T^(ur // g))^g    )
        if alpha^N == Q^(N * ur):
            rank += g

    return list_factors_extension, rank



