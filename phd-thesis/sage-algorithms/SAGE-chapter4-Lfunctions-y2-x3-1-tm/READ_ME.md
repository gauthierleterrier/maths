This folder contains files (plus this "read me"), related to the PhD thesis "Some Mordell-Weil lattices and applications to sphere packings".

The files can be run on SAGE to compile and test the algorithms (for instance, by uploading these files on [cocalc.com](https://cocalc.com/)).

0) The main functions and code are given in the file "L_functions_y2_x3_1_tm.sage"

1) The file "Some examples of L-functions and analytic ranks.ipynb" gives computations of analytic rank and L-functions. 

2) The file "q_7__table_Jacobi_sums__explanations.ipynb" explains how the various tables (contained in the subfolder `Data_Table_List_Jacobi_sums`) are obtained.

3) The files "q = 7 - List or table of analytic ranks (conjecture 4.2.30).ipynb" (and similary for q = 13, q = 19, q = 1801) provide some evidence towards conjecture 4.2.30. The tables of analytic ranks use the results listed in "q_7__table_Jacobi_sums.sage"

4) The file "Case q = -1 mod 3.ipynb" gives examples of computations where the field of constants are size q = -1 mod 3.

5) There are some extra files, related to specific places in the text of the thesis :
	- Upper bound rank for q = 7 (Example 4.2.25).ipynb
	- Degree of field extension over which the geometric rank is attained (Remark 4.2.28).ipynb
	- Proof of theorem 4.2.1 - conclusion from equation 4.2.17.ipynb


Author:	Gauthier LETERRIER
Date:	2023-05-09 (YYYY-MM-DD)
Contact:	gauthier (dot) leterrier (at) gmail (dot) com