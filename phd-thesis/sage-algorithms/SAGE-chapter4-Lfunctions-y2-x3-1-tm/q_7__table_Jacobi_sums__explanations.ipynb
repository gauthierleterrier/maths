{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "#### This file explains the results and data from the file `q_7__table_Jacobi_sums.sage`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "We want to compute the Jacobi sums occuring in the L-function of $E'_{m,b,b'} : y^2 = x^3 + b + b' t^m$ over $k(t)$ as in theorem 4.1.2, namely $J(\\psi, \\lambda, \\theta_{k_{u(r)}, m, r})$ where $\\psi$ is a character of order 3 on $k_{u(r)}^\\times$ (introduced in Definition 4.1.1), $\\lambda$ denotes the Legendre symbol and $u(r) = u_{|k|, m}(r)$.\n",
    "We briefly explain the idea to make the computation of the Jacobi sums more efficient. See the function `Jacobi_psi3_Leg_chi(q, r, m)` in the file `q_7__table_Jacobi_sums.sage`.\n",
    "\n",
    "This triple Jacobi sum $J(\\psi, \\lambda, \\theta_{k_{u(r)}, m, r})$ is a sum over $q^2$ terms, where $q := |k|^{u(r)}$. Even when $Q := |k| = 7$ and $m = 10, r = 1$, we have $u_{Q, m}(r) = 4$ so $q^2 = (7^4)^2 \\simeq 5.7 \\cdot 10^6$ terms, which would take already several minutes (or hours) to compute!\n",
    "\n",
    "Instead, we make use of Equation 4.1.5 in order to re-write this triple Jacobi sum as a product of two double Jacobi sums, each of them being a sum over (only) $q$ terms, which means that we now have $2q$ terms instead of $q^2$: this is a big improvement!\n",
    "\n",
    "Still, it is important to discuss how to compute each of these two sums. Overall, both sums run over $k_{u(r)}$ and we list the non-zero elements of this finite field as a power $g^e$ of some generator $g$ of the multiplicative group $k_{u(r)}^\\times$.\n",
    "\n",
    "  - The first Jacobi sum is $$J(\\psi, \\lambda) = \\sum_{x \\in k_{u(r)}} \\psi(x) \\lambda(1 - x),$$ where $\\psi$ has order 3 and $\\lambda$ is the Legendre symbol. When $x=g^e$, the value $\\psi(x) = E(3)^e$ is easily computed, where $E(3) := \\exp(2 \\pi i / 3)$ (see also Remark 1.4.18). Then $\\lambda(1 - x)$ is also easily computed using the fact that $\\lambda(y) \\equiv y^{ (q-1) / 2 }$.\n",
    "\n",
    "  - The first Jacobi sum is $$J(\\psi \\lambda, \\theta) = J(\\theta, \\psi\\lambda) = \\sum_{x \\in k_{u(r)}} \\theta(x) \\cdot (\\psi \\lambda)(1 - x),$$ where $\\psi \\lambda$ has order 6 and $\\theta = \\theta_{k_{u(r)}, m, r}$ has order $m / \\gcd(m, r)$. When $x = g^e$, we have $\\theta(x) = E(m)^{ r \\cdot e }$, where $E(m) := \\exp(2 \\pi i / m)$. Now $\\psi \\lambda$ is a character taking value $E(6)^{ -1 }$ on the generator $g$. In general, computing $\\psi \\lambda(1 - x)$ would require to solve a **discrete logarithm problem** in $k_{u(r)}^{\\times}$. But in fact, since $\\psi \\lambda$ has order 6, we can test which of the six values $E(6)^e$ (with $0 \\leq e < 6$) is equal to $(\\psi \\lambda)(1 - x)$. The point is that if $1 - x = g^\\alpha$ then $(\\psi \\lambda)(1 - x) = E(6)^\\alpha$ and we only need to know the value of $\\alpha$ modulo 6, which can be found by computing $y := (1 - x)^{(q-1) / 6} = g_1^{\\alpha \\bmod 6}$ where $g_1 := g^{(q-1) / 6}$. This computation is done in the function `psi6_(q, x, g, g1)` from the file `q_7__table_Jacobi_sums.sage`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "----------------\n",
    "In what follows, we work over $\\mathbb{F}_7$, so $Q = 7$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
   ],
   "source": [
    "import time\n",
    "load('L_functions_y2_x3_1_tm.sage')\n",
    "Q = 7"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "We first do a small example with $m = 10$ and $r = 1$. Here the notation $\\texttt{E(n)}$ means $\\exp(2 \\pi i / n) \\in \\overline{\\mathbb Q}$, for any integer $n \\geq 1$. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Size of the finite field over which the characters are defined: 2401\n"
     ]
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "The two Jacobi sums are (-55*E(3) - 39*E(3)^2, 56*E(3) + 35*E(3)^2) \n",
      "\n",
      "Time:  1.5906  seconds\n"
     ]
    }
   ],
   "source": [
    "r = 1\n",
    "m = 10\n",
    "q = Q^(u(Q, m, r))\n",
    "print(\"Size of the finite field over which the characters are defined:\", q)\n",
    "\n",
    "t0 = time.time()\n",
    "print(\"The two Jacobi sums are\", Jacobi_psi3_Leg_chi(q, r, m), \"\\n\")\n",
    "t1 = time.time()\n",
    "print(\"Time: \", round(t1 - t0, 4), \" seconds\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "We now give an example with $m = 36$ and $r = 1$. Here $u_{Q, m}(r) = 6$ so we have Jacobi sums of characters defined over $\\mathbb{F}_{7^6}$, a field of size 117649, which is quite big. Despite our improvements explained above, it takes roughly 30 to 80 seconds to compute just these two Jacobi sums... \n",
    "Recall that computing the L-function, or the analytic rank, of $E'_{m,b,b'}$ over $k(t)$, requires to compute these sums for $r \\in X(m, 1) / \\langle |k| \\rangle$ (see theorem 4.1.2), which is (roughly) of size $m$, so 30 seconds times $m=36$ already gives almost 20 minutes!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Size of the finite field over which the characters are defined: 117649\n"
     ]
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "The two Jacobi sums are (37*E(3) - 323*E(3)^2, -385*E(3) - 112*E(3)^2) \n",
      "\n",
      "Time:  74.322  seconds to compute a single triple Jacobi sum!\n"
     ]
    }
   ],
   "source": [
    "r = 1\n",
    "m = 36\n",
    "q = Q^(u(Q, m, r))\n",
    "print(\"Size of the finite field over which the characters are defined:\", q)\n",
    "\n",
    "t0 = time.time()\n",
    "print(\"The two Jacobi sums are\", Jacobi_psi3_Leg_chi(q, r, m), \"\\n\")\n",
    "t1 = time.time()\n",
    "print(\"Time: \", round(t1 - t0, 4), \" seconds to compute a single triple Jacobi sum!\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "---\n",
    "Now we give a list of Jacobi sums where $r$ varies in $\\Z / m \\Z \\setminus \\{ 0 \\}$ (as in the product expressing the L-function of $E'_{m,b,b'}$ in theorem 4.1.2). For $m=10$, we have a list of 9 items of the form $[ r, (Jacobi1, Jacobi2) ]$ where $Jacobi1 = J(\\psi, \\lambda)$, $Jacobi2 = J(\\psi \\lambda, \\theta_{k_{u(r)}, m, r})$ and $\\psi, \\lambda$ are defined over $k_{u(r)}$ and $k = \\mathbb F_7, u(r) := u_{7, m}(r)$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[[1, (-55*E(3) - 39*E(3)^2, 56*E(3) + 35*E(3)^2)],\n",
       " [2, (-55*E(3) - 39*E(3)^2, 16*E(3) - 39*E(3)^2)],\n",
       " [3, (-55*E(3) - 39*E(3)^2, 56*E(3) + 35*E(3)^2)],\n",
       " [4, (-55*E(3) - 39*E(3)^2, 16*E(3) - 39*E(3)^2)],\n",
       " [5, (-E(3) - 3*E(3)^2, 3*E(3) + E(3)^2)],\n",
       " [6, (-55*E(3) - 39*E(3)^2, 16*E(3) - 39*E(3)^2)],\n",
       " [7, (-55*E(3) - 39*E(3)^2, 56*E(3) + 35*E(3)^2)],\n",
       " [8, (-55*E(3) - 39*E(3)^2, 16*E(3) - 39*E(3)^2)],\n",
       " [9, (-55*E(3) - 39*E(3)^2, 56*E(3) + 35*E(3)^2)]]"
      ]
     },
     "execution_count": 6,
     "metadata": {
     },
     "output_type": "execute_result"
    }
   ],
   "source": [
    "m = 10\n",
    "list_jacobi_sums(m , Q)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "Now we give a code that gives, for each $1 \\leq m \\leq 50$ coprime to $Q = 7$, the list of Jacobi sums over $r \\in \\mathbb Z / m \\mathbb Z$ as in the above example. The part with $m \\leq 30$ compiles in roughly 3 minutes, while running over $31 \\leq m \\leq 50$ takes up to 2 hours. The results are stored in the file \"q_7__table_Jacobi_sums.sage\"."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
   ],
   "source": [
    "for m in range(1, 31):\n",
    "    if gcd(m, Q) == 1 and u(Q, m, 1) <= 6:#We remove those integers m for which would take too much time for the computation to run\n",
    "        print(\"\\n\", m, \"  &  \", list_jacobi_sums(m, Q), \"\\\\\\\\\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
   ],
   "source": [
    "for m in range(31, 51):\n",
    "    if gcd(m, Q) == 1 and u(Q, m, 1) <= 6:#We remove those integers m for which would take too much time for the computation to run\n",
    "        print(\"\\n\", m, \"  &  \", list_jacobi_sums(m, Q), \"\\\\\\\\\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "---\n",
    "In what follows we indicate, for each $m \\leq 50$ and $m = 60, 360$, the degree $u(Q, m, 1)$ of the field extension of $\\mathbb F_Q = \\mathbb F_7$ over which the characters are defined to compute the Jacobi sums appearing in the L-function of $y^2 = x^3 + 1 + t^m$ over $\\mathbb F_Q(t)$. When the degree is strictly more that 6, the extension would be too big for the computation of the Jacobi sums to run in a reasonable time. This happen for the following values of $m$: 11, 13, 17, 22, 23, 26, 27, 29, 31, 33, 34, 37, 39, 41, 44, 45, 46, 47, 360. \n",
    "\n",
    "Luckily, for $m \\in \\{ 11, 13, 17, 23, 29, 31, 37, 41, 47 \\}$, we note that $m$ is coprime to 30, so the geometric rank of $y^2 = x^3 + 1 + t^m$ is 0 by Corollary 4.2.23.  So the smallest value of $m$ where we cannot compute exactly the rank of $y^2 = x^3 + 1 + t^m$ over $\\mathbb F_7(t)$ is $m = 22$ (then come $m = 26, 27, 33, 34, 39, 44, 45, 46, 360$)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "m   u(Q, m, 1) \n",
      " ==============================\n",
      "1 \t 1 \t ok\n",
      "2 \t 1 \t ok\n",
      "3 \t 1 \t ok\n",
      "4 \t 2 \t ok\n",
      "5 \t 4 \t ok\n",
      "6 \t 1 \t ok\n",
      "8 \t 2 \t ok\n",
      "9 \t 3 \t ok\n",
      "10 \t 4 \t ok\n",
      "11 \t 10 \t too big\n",
      "12 \t 2 \t ok\n",
      "13 \t 12 \t too big\n",
      "15 \t 4 \t ok\n",
      "16 \t 2 \t ok\n",
      "17 \t 16 \t too big\n",
      "18 \t 3 \t ok\n",
      "19 \t 3 \t ok\n",
      "20 \t 4 \t ok\n",
      "22 \t 10 \t too big\n",
      "23 \t 22 \t too big\n",
      "24 \t 2 \t ok\n",
      "25 \t 4 \t ok\n",
      "26 \t 12 \t too big\n",
      "27 \t 9 \t too big\n",
      "29 \t 7 \t too big\n",
      "30 \t 4 \t ok\n",
      "31 \t 15 \t too big\n",
      "32 \t 4 \t ok\n",
      "33 \t 10 \t too big\n",
      "34 \t 16 \t too big\n",
      "36 \t 6 \t ok\n",
      "37 \t 9 \t too big\n",
      "38 \t 3 \t ok\n",
      "39 \t 12 \t too big\n",
      "40 \t 4 \t ok\n",
      "41 \t 40 \t too big\n",
      "43 \t 6 \t ok\n",
      "44 \t 10 \t too big\n",
      "45 \t 12 \t too big\n",
      "46 \t 22 \t too big\n",
      "47 \t 23 \t too big\n",
      "48 \t 2 \t ok\n",
      "50 \t 4 \t ok\n",
      "60 \t 4 \t ok\n",
      "120 \t 4 \t ok\n",
      "180 \t 12 \t too big\n",
      "360 \t 12 \t too big\n",
      "\n",
      " List of m with degree u(Q, m, 1) being > 6 : [22, 26, 27, 33, 34, 39, 44, 45, 46, 360]\n"
     ]
    }
   ],
   "source": [
    "def size(u):\n",
    "    if u > 6: return \"too big\"\n",
    "    return \"ok\"\n",
    "\n",
    "print(f\"m   u(Q, m, 1) \\n\", \"=\"*30)\n",
    "for m in srange(1, 51) + [60, 120, 180, 360]:\n",
    "    if gcd(m, Q) == 1:\n",
    "        print(m, \"\\t\", u(Q, m, 1), \"\\t\", size(u(Q, m, 1)))\n",
    "\n",
    "print(\"\\n List of m with degree u(Q, m, 1) being > 6 :\", [m for m in srange(1, 51) + [60, 360] if gcd(m, Q) == 1 and u(Q, m, 1) > 6 and gcd(m, 30) > 1])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
   ],
   "source": [
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "argv": [
    "sage-9.8",
    "--python",
    "-m",
    "sage.repl.ipython_kernel",
    "--matplotlib=inline",
    "-f",
    "{connection_file}"
   ],
   "display_name": "SageMath 9.8",
   "env": {
   },
   "language": "sagemath",
   "metadata": {
    "cocalc": {
     "description": "Open-source mathematical software system",
     "priority": 10,
     "url": "https://www.sagemath.org/"
    }
   },
   "name": "sage-9.8",
   "resource_dir": "/ext/jupyter/kernels/sage-9.8"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}