
"""
This is the file "L_functions_y2_x3_x_tm.sage". It can be used to study the L-function, analytic rank and geometric rank of the curves y^2 = x^3 + b x + b' t^m over F_q(T) where q is a power of an odd prime p. See chapter 3 in the thesis.
"""

CycloT.<T> = PolynomialRing(UniversalCyclotomicField())
import time

def d(m):
    """
    See definition 3.1.1
    """
    return 4*m / gcd(2, m)


def Z(m):
    """
    See definition 3.1.1
    """
    dm = d(m)
    if m % 2 == 0 and m % 6 != 0:
        return [mod(x, dm) for x in range(dm) if x % (m//2) != 0]
    if m % 6 == 0:
        return [mod(x, dm) for x in range(dm) if (x % (m//2) != 0 and x % (2*m//3) != 0)]
    else:
        return [mod(x, dm) for x in range(dm) if (x % 2 != 0 and x % m != 0)]




def u(q, d, r):
    """
    INPUT
        - A prime power q 
        - An integer d ≥ 1 coprime to q
        - An element r in {1, ..., d} (or in Z / d Z)

    OUTPUT
        - The multiplicative order of q modulo d / gcd(d, r), as in definition 1.4.15
    """    D = d // gcd(d, r)
    return mod(q, D).multiplicative_order()



def Repres_Orbits(m, Q):
    """
    INPUT
        - An integer m ≥ 1
        - A prime power Q (typically such that our elliptic curve is defined over F_Q(t)
        - A subset "set_X" of {0, ..., m - 1} (or of Z / m Z)

    OUTPUT
        - List of representatives for the quotient of set_X by the powers of Q acting on Z / m Z
    """
    Repres = []
    Union_orbits = []
    dm = d(m)
    orderQ = mod(Q, dm).multiplicative_order()
    for x in Z(m):
        if x not in Union_orbits:
            Union_orbits += list({Q^j * x for j in range(orderQ)})
            Repres += [x]
    return Repres



### Characters

def Legendre(Q, x):
    """
    INPUT
        - A power Q of an odd prime
        - An element x of GF(Q)

    OUTPUT
        - The value of Legendre symbol of GF(Q) at x
    """
    if x==0: return 0
    value = GF(Q)(x^( (Q-1) // 2))
    if value == -1: return -1
    else:           return  1




def theta(Q, n, d, r):
    """
    INPUT
        - A prime power Q
        - An integer d ≥ 1
        - An element r in {1, ..., d}
        - An integer n ≥ 1 which is a multiple of u(q, d, r) as defined above

    OUTPUT
        - Returns the multiplicative character on F_{Q^n}, introduced in Definition 1.4.15 , of order d / gcd(d, r)
    (see also Remark 1.4.18).
    """
    ur = u(Q, d, r)
    assert(n % ur == 0)
    def my_theta(x):
        if x == 0:
            if r == 0: return 1
            else:      return 0
        kn = GF(Q^n)
        g = kn.multiplicative_generator()
        e = discrete_log(x, g, Q^n - 1, operation='*')
        Theta_of_x = E(Q^n - 1)^e #= exp(2*pi*I*e / (Q^n - 1)) #Teichmuller char at x, for x \in GF(Q^n).
        return Theta_of_x^( (Q^n - 1)*r // d )
    return my_theta




def two_jacobi_sums(p, Q, n, d, r, verbose=False):
    """
    See definition 3.1.1 : computes the two Jacobi sums appearing in "alpha".
    """
    jacobi_1 = 0#Jacobi(Q^n, Leg(Q^n), mult(Leg(Q^n), chi_2))
    jacobi_2 = 0#Jacobi(Q^n, chi, chi_2)
    G1, G2, G3 = 0, 0, 0
    x = GF(Q^n)(1)
    g = GF(Q^n).multiplicative_generator()
    zeta_d = E(d)^r
    zeta_p = E(p)
    if verbose: print("Running over a finite field of size", Q^n)
    for e in range(Q^n - 1):
        if verbose and e % (10^4) == 0: print("Running step e =", e, " ...")
        e1 = e % (2*d)
        jacobi_1 += (  (-1)^e1 * zeta_d^(2*e1)  *   Legendre(Q^n, 1 - x)  )#zeta_d^(2*e) = chi_r^2(x)
        zeta_t = zeta_p^(x.trace().lift())
        chi_x  = E( d )^(r*e1)
        G1 += (chi_x   * zeta_t)
        G2 += (chi_x^2 * zeta_t)
        G3 += (chi_x^3 * zeta_t)
        x *= g
    jacobi_2 = G1 * G2 / G3
    return jacobi_1 , jacobi_2


### L-function and analytic rank


def epsilon(m, b, b2, Q):
    """
    See definition 3.1.1
    """
    if m % 2 == 0 and Legendre(Q, -b) == 1 and Legendre(Q, b2) == 1:
        return (1 - Q*T)^2
    if m % 2 == 0 and Legendre(Q, -b) == 1 and Legendre(Q, b2) == -1:
        return (1 + Q*T)^2
    if m % 2 == 0 and Legendre(Q, -b) == -1:
        return 1 - (Q*T)^2
    if m % 2 == 1:
        return 1


def alpha(b1, b2, p, Q, n, d, r, chi):
    """
    See definition 3.1.1
    """
    return Legendre(Q^n, -b2) * chi(-b2^(-2) * b1^3) * prod(two_jacobi_sums(p, Q, n, d, r))





def LFunction(m, b1, b2, Q):
    assert(gcd(m, Q) == 1)
    p = factor(Q)[0][0]
    L = epsilon(m, b, b2, Q)
    dm = d(m)
    for r0 in Repres_Orbits(m, Q):
        r = r0.lift()
        ur = u(Q, dm, r)
        chi_r = theta(Q, ur, dm, r)
        L *= (1 - alpha(b1, b2, p, Q, ur, dm, r, chi_r) * T^(ur))
    return L



def analytic_rank(m, b1, b2, Q):
    assert(gcd(m, Q) == 1)
    rank = 0
    p = factor(Q)[0][0]
    if m % 2 == 0 and Legendre(Q, -b1) == 1 and Legendre(Q, b2) == 1:
        rank += 2
    if m % 2 == 0 and Legendre(Q, -b1) == -1:
        rank += 1
    dm = d(m)
    for r0 in Repres_Orbits(m, Q):
        r = r0.lift()
        ur = u(Q, dm, r)
        chi_r = theta(Q, ur, dm, r)
        if alpha(b1, b2, p, Q, ur, dm, r, chi_r) == Q^ur:
            rank += 1
    return rank

