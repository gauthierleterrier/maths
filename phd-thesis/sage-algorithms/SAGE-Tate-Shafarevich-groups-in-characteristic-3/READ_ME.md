This folder contains 5 files (plus this "read me"), related to the PhD thesis "Some Mordell-Weil lattices and applications to sphere packings".

The 5 files can be run on SAGE to compile and test the algorithms (for instance, by uploading these files on cocalc.com).


1) The file "Kernel of phi and explicit descent map.ipynb" contains computations related to propositions 3.4.7 and 3.4.12.

2) The file "Tate-Shafarevich group - case n = 2 in Theorem 3.4.1.ipynb" contains computations related to the proof of theorem 3.4.1, case n = 2 (page 189).

3) The file "Tate-Shafarevich group - case n = 3 and deg(y^_) = 7.ipynb" contains computations related to the proof of theorem 3.4.1, case n = 3 (pages 190-191).

4) The file "Tate-Shafarevich group - case n = 3 and deg(y^_) = 10.ipynb" contains computations related to the proof of theorem 3.4.1, case n = 3 (page 190).

5) The file "Tate-Shafarevich group - case n = 3 and deg(y^_) = 13.ipynb" contains computations related to the proof of theorem 3.4.1, case n = 3 (pages 189-190).



Author:	Gauthier LETERRIER
Date:	2023-05-03 (YYYY-MM-DD)
Contact:	gauthier (dot) leterrier (at) gmail (dot) com