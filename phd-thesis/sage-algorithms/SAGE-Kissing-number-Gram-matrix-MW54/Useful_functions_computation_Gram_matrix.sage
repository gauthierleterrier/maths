"""
This file contains the main functions used to compute a Gram matrix of the lattice L'_{3, 1} := E(K)^0
where E : y^2 = x^3 + x + t^28 and K = F_{3^6}(t). We refer to section 3.3.2.
"""

import time

#We first define some invariants. The rank of L'_{3,1} is 2 * 3^n = 54, where n := 3.
n = 3
dim = 2*3^n
q = 3^n
Q = q^2
b = 1
k.<z> = GF(Q)
z     = k.gen()
K.<t> = FunctionField(k)
R.<t> = PolynomialRing(k)
E     = EllipticCurve(K, [0, 0, 0, b, t^(q + 1)])
g     = k.multiplicative_generator()


def height_naive(P):
    """
    Input:  A rational point P on the elliptic curve E
    Output: The naive height of P
    Note:   By Corollary 3.2.18, it coincides with the Néron-Tate height on the narrow Mordell-Weil lattice E(K)^0
    """
    return max(P[0].numerator().degree() , P[0].denominator().degree())


def pairing(P, Q):
    """
    Input:  Two rational points P, Q on the elliptic curve E
    Output: The Néron-Tate height pairing of P and Q
    """
    return (height_naive(P + Q) - height_naive(P) - height_naive(Q)) / 2


def Gram_matrix(list_points):
    """
    Input:  A finite list "list_points" of rational points on the elliptic curve E
    Output: The Gram matrix of "list_points" with respect to the Néron-Tate height pairing
    """
    r = len(list_points)
    G = matrix(QQ, r)
    for j in range(r):
        G[j, j] = height_naive(list_points[j])
    for i in range(r):
        for j in range(i + 1, r):
                G[i, j] = (height_naive(list_points[i] + list_points[j]) - G[i, i] - G[j, j]) / 2
                G[j, i] = G[i, j]
    return G



def coeff(w, basis, Gram):
    """
    Input:  A rational point w on E, a list "basis" of rational points on E with Gram matrix given by "Gram"
    Output: The list of coefficients of w written as a linear combination of points in the "basis", as in lemma 3.3.9
    """
    vect = vector([pairing(vi, w) for vi in basis])
    return Gram^(-1) * vect



def frac_0(x):
    """
    Input:  A real number x
    Output: The unique real number y such that y - x is an integer and -1/2 < y ≤ 1/2
    """
    y = frac(x)
    if y > 1/2: return y - 1
    return y



def change_basis(A_total, Q, alpha0, verbose = 0):
    """
    Input:
        - A matrix "A_total" (matrix of a basis of a sublattice with respect to an original "basis_0")
        - A rational point Q on E
        - The list of coefficients "alpha0" of Q with respect to an original "basis_0"
        - verbose: if 0, nothing is printed; if > 0, it tells us which point P_r gets replaced by a new point.
    Output: The updated matrix A_total (of a new basis with respect to "basis_0")
    """
    alpha = A_total.transpose()^(-1) * alpha0
    not_integral = [j for j in range(dim) if not alpha[j] in ZZ]
    if len(not_integral) > 0:
        A_local = identity_matrix(QQ, dim)
        r = not_integral[0]
        #If some coefficient alpha_r is non-zero, then replace P_r by Q' as explained in §3.3.2.3
        if verbose > 0:  print(f"Processing... replace: P{r} by Q.")
        for j in range(dim):
            if j in not_integral: A_local[r, j] = frac_0(alpha[j])
        A_total = A_local*A_total
    else:
        if verbose > 0: print("The point Q is already in the sublattice.")
    return A_total







def random_point(verbose = 0):
    """
    Input:
        - verbose=0 does not print anything
        - verbose=1 prints a list of 5 items [e3, e6, e9, e12, e15]:
              > if e_i is an integer, then we set y_i = g^{e_i} where g is a multiplicative generator of F_{3^6}^{\times};
              > if e_i = "" is the empty string, then we set y_i = 0.
          This gives us five coefficients y_3, y_6, y_9, y_{12}, y_{15} from which a polynomial y(t) and then a rational point Q = (x, y) on E can be deduced (which is the output)
       - verbose=2 prints the rational point Q itself

    Output: A rational point Q = (x, y) on E of height 10, with random coefficients y_12 and y_15 (and y_6, y_9).
    """
    #We use the "case 2" given in equation 3.3.10. The equations f4, f5, f7, f8 below are coming from the file "Explanation of the formulas.ipynb". In principle, to ensure that a rational point exists with a given y-coordinate, one should also check that f1 = f2 = 0 and that y0^2 = s0^3 + s0 for some s0 in k, but these are apparently automatically satisfied.
    R0.<Y9, Y6> = PolynomialRing(k)
    solution = 0
    while solution == 0:
        e15 = randint(0, q^2)
        Y15 = g^e15
        Y12 = k.random_element()
        Eq  = [#f5
               -Y9^3*Y12^3*Y15^1527 - Y6^3*Y15^1530 - Y12^3*Y15^1512 + Y12^11*Y15^72 - Y9*Y12^9*Y15^73 - Y9^3*Y12^5*Y15^75 + Y9^4*Y12^3*Y15^76 + Y12^9*Y15^68 + Y9*Y12^7*Y15^69 + Y6*Y12^6*Y15^70 - Y9^3*Y12^3*Y15^71 - Y9^4*Y12*Y15^72 - Y6*Y9^3*Y15^73 - Y12^27*Y15^48 + Y9^3*Y12^21*Y15^51 + Y6^3*Y12^18*Y15^54 - Y9^12*Y12^3*Y15^60 - Y6^3*Y9^9*Y15^63 - Y6^9*Y15^66 + Y12^7*Y15^64 - Y9^3*Y12*Y15^67 + Y12^5*Y15^60 - Y9*Y12^3*Y15^61 + Y12^3*Y15^56 + Y9*Y12*Y15^57 + Y6*Y15^58 + Y12^21*Y15^36 - Y9^3*Y12^15*Y15^39 - Y9^6*Y12^9*Y15^42 - Y6^3*Y12^12*Y15^42 + Y9^9*Y12^3*Y15^45 - Y6^3*Y9^3*Y12^6*Y15^45 - Y6^3*Y9^6*Y15^48 + Y12*Y15^52 - Y12^15*Y15^24 + Y9^6*Y12^3*Y15^30 + Y6^3*Y12^6*Y15^30 - Y6^3*Y9^3*Y15^33 - Y12^9*Y15^12 + Y6^3*Y15^18 + Y12^3,
            #f7
            (Y12^9*Y15^1512 + Y6^3*Y15^1518 - Y12^3*Y15^1500 - Y9*Y12^7*Y15^57 - Y6*Y12^6*Y15^58 + Y9^4*Y12*Y15^60 + Y6*Y9^3*Y15^61 - Y6^3*Y12^18*Y15^42 + Y9^9*Y12^9*Y15^45 + Y6^3*Y9^9*Y15^51 + Y6^9*Y15^54 - Y12^7*Y15^52 + Y9^3*Y12*Y15^55 - Y9*Y12*Y15^45 - Y6*Y15^46 + Y12^21*Y15^24 + Y9^3*Y12^15*Y15^27 + Y9^6*Y12^9*Y15^30 + Y6^3*Y12^12*Y15^30 + Y6^3*Y9^3*Y12^6*Y15^33 + Y6^3*Y9^6*Y15^36 - Y12*Y15^40 + Y12^15*Y15^12 - Y9^6*Y12^3*Y15^18 - Y6^3*Y12^6*Y15^18 + Y6^3*Y9^3*Y15^21 - Y12^9 - Y9^3*Y12^3*Y15^3 - Y6^3*Y15^6),
            #f8
            Y15^1512 + Y12^2*Y15^60 - Y9*Y15^61 + Y15^56 - Y12^18*Y15^36 + Y9^9*Y15^45 + 1,
            #f4
            Y15^3009 - Y9^3*Y12^12*Y15^1536 - Y6^3*Y12^9*Y15^1539 - Y6^3*Y9^3*Y12^3*Y15^1542 - Y6^6*Y15^1545 - Y9^3*Y12^6*Y15^1524 + Y9^6*Y15^1527 + Y9^3*Y15^1512 - Y15^1497 - Y12^12*Y15^569 - Y9^3*Y12^6*Y15^572 - Y9^6*Y15^575 - Y12^10*Y15^565 - Y9*Y12^8*Y15^566 - Y9^2*Y12^6*Y15^567 + Y9^3*Y12^4*Y15^568 + Y9^4*Y12^2*Y15^569 + Y9^5*Y15^570 + Y12^8*Y15^561 - Y9*Y12^6*Y15^562 - Y9^3*Y12^2*Y15^564 + Y9^4*Y15^565 - Y12^4*Y15^553 - Y9*Y12^2*Y15^554 - Y9^2*Y15^555 + Y12^2*Y15^549 - Y9*Y15^550 + Y15^545 - Y12^18*Y15^77 - Y9*Y12^16*Y15^78 - Y6*Y12^15*Y15^79 - Y9^4*Y12^10*Y15^81 - Y6*Y9^3*Y12^9*Y15^82 - Y9^7*Y12^4*Y15^84 - Y6*Y9^6*Y12^3*Y15^85 + Y9^9*Y15^86 + Y12^36*Y15^57 - Y6^3*Y12^27*Y15^63 - Y9^9*Y12^18*Y15^66 + Y6^3*Y9^3*Y12^21*Y15^66 - Y9^12*Y12^12*Y15^69 + Y6^6*Y12^18*Y15^69 - Y6^3*Y9^9*Y12^9*Y15^72 - Y9^18*Y15^75 - Y6^3*Y9^12*Y12^3*Y15^75 - Y6^9*Y12^9*Y15^75 - Y6^6*Y9^9*Y15^78 - Y6^9*Y9^3*Y12^3*Y15^78 + Y6^12*Y15^81 + Y9*Y12^14*Y15^74 - Y9^3*Y12^10*Y15^76 + Y6*Y9*Y12^11*Y15^76 + Y6*Y9^2*Y12^9*Y15^77 - Y6^2*Y12^10*Y15^77 - Y9^5*Y12^6*Y15^78 + Y6*Y9^3*Y12^7*Y15^78 - Y6^2*Y9*Y12^8*Y15^78 + Y9^6*Y12^4*Y15^79 - Y6*Y9^4*Y12^5*Y15^79 - Y6^2*Y9^2*Y12^6*Y15^79 - Y9^7*Y12^2*Y15^80 - Y6*Y9^5*Y12^3*Y15^80 + Y6^2*Y9^3*Y12^4*Y15^80 + Y9^8*Y15^81 - Y6*Y9^6*Y12*Y15^81 + Y6^2*Y9^4*Y12^2*Y15^81 + Y6^2*Y9^5*Y15^82 + Y12^14*Y15^69 - Y9^2*Y12^10*Y15^71 + Y6*Y12^11*Y15^71 + Y9^3*Y12^8*Y15^72 + Y9^4*Y12^6*Y15^73 - Y6*Y9^2*Y12^7*Y15^73 + Y6^2*Y12^8*Y15^73 + Y9^5*Y12^4*Y15^74 - Y6*Y9^3*Y12^5*Y15^74 - Y6^2*Y9*Y12^6*Y15^74 + Y9^6*Y12^2*Y15^75 - Y9^7*Y15^76 + Y6*Y9^5*Y12*Y15^76 - Y6^2*Y9^3*Y12^2*Y15^76 + Y6^2*Y9^4*Y15^77 + Y12^12*Y15^65 - Y9*Y12^10*Y15^66 - Y9^2*Y12^8*Y15^67 - Y6*Y9*Y12^7*Y15^68 + Y9^4*Y12^4*Y15^69 - Y6^2*Y12^6*Y15^69 + Y9^5*Y12^2*Y15^70 - Y9^6*Y15^71 + Y6*Y9^4*Y12*Y15^71 + Y6^2*Y9^3*Y15^72 + Y12^30*Y15^45 + Y9^3*Y12^24*Y15^48 + Y9^6*Y12^18*Y15^51 - Y6^3*Y12^21*Y15^51 - Y6^3*Y9^3*Y12^15*Y15^54 - Y6^3*Y9^6*Y12^9*Y15^57 + Y6^6*Y12^12*Y15^57 + Y6^6*Y9^3*Y12^6*Y15^60 + Y6^6*Y9^6*Y15^63 - Y12^10*Y15^61 - Y9^2*Y12^6*Y15^63 + Y6*Y12^7*Y15^63 + Y6*Y9*Y12^5*Y15^64 - Y9^4*Y12^2*Y15^65 + Y6*Y9^2*Y12^3*Y15^65 - Y6^2*Y12^4*Y15^65 - Y6^2*Y9*Y12^2*Y15^66 - Y6^2*Y9^2*Y15^67 - Y9^2*Y12^4*Y15^59 + Y6*Y12^5*Y15^59 + Y9^4*Y15^61 - Y6*Y9^2*Y12*Y15^61 + Y6^2*Y12^2*Y15^61 - Y6^2*Y9*Y15^62 + Y12^6*Y15^53 - Y9^2*Y12^2*Y15^55 + Y6*Y12^3*Y15^55 + Y9^3*Y15^56 - Y6*Y9*Y12*Y15^56 - Y6^2*Y15^57 - Y12^24*Y15^33 + Y9^3*Y12^18*Y15^36 + Y9^6*Y12^12*Y15^39 - Y6^3*Y12^15*Y15^39 + Y9^9*Y12^6*Y15^42 + Y9^12*Y15^45 + Y6^3*Y9^6*Y12^3*Y15^45 - Y6^6*Y12^6*Y15^45 + Y6^6*Y9^3*Y15^48 - Y12^4*Y15^49 - Y9*Y12^2*Y15^50 - Y9^2*Y15^51 + Y6*Y12*Y15^51 - Y12^2*Y15^45 - Y15^41 + Y12^18*Y15^21 - Y9^3*Y12^12*Y15^24 + Y9^6*Y12^6*Y15^27 - Y6^3*Y12^9*Y15^27 + Y9^9*Y15^30 - Y9^3*Y12^6*Y15^12 - Y9^6*Y15^15 + Y6^3*Y12^3*Y15^15 + Y9^3
              ]
        Eq = [R0(fi) for fi in Eq]

        J1 = R0.ideal(Eq)
        V1 = J1.variety()

        if len(V1) > 0:
            y6 = V1[0][Y6]
            y9 = V1[0][Y9]
            y12= k(Y12)
            y15= k(Y15)
            if y12^2*y15^4 - y9*y15^5 + 1 != 0:
                y3 = -y15^(-11) * (y12^2*y15^4 - y9*y15^5 + 1)^(-1) * (y15^504 - y12^6*y15^12 + y6*y12^3*y15^14 - y9^3*y15^15 + y6*y9*y12*y15^15 + y6^2*y15^16 - y9*y12^2*y15^9 - y9^2*y15^10 - y12^2*y15^4 - y9*y15^5 - 1)

                y0 = (-y12^5*y15^8 + y9*y12^3*y15^9 - y6*y9*y15^11 - y3*y12*y15^11 + y9*y12*y15^5 + y6*y15^6 - y12)/y15^12
                y1 = (-y12^4*y15^8 - y9^2*y15^10 + y6*y12*y15^10 + y3*y15^11 + y12^2*y15^4 - y9*y15^5 - 1)/y15^13
                y2 = (-y12^3)/y15^6
                y4 = (y12^3*y15^4 + y9*y12*y15^5 + y6*y15^6 - y12)/y15^8
                y5 = 1/y15^9
                y7 = (-y12^2*y15^4 + y9*y15^5 + 1)/y15^7
                y8 = 0
                y10 = y12/y15^2
                y11 = 1/y15^3
                y13 = (-1)/y15
                y14 = 0

                #The y-coordinate is:
                Py = K(y0 + y1*t^1 + y2*t^2 + y3*t^3 + y4*t^4 + y5*t^5 + y6*t^6 + y7*t^7 + y8*t^8 + y9*t^9 + y10*t^10 + y11*t^11 + y12*t^12 + y13*t^13 + y14*t^14 + y15*t^15)

                #List of rational points on E with y-coordinate equal to Py:
                solu     = lift_y(Py)
                solution = len(solu)

    #We pick one such rational point (the list "solu" should not be empty, in principle)
    Q = solu[0]
    SOL = [discretelog(Y, g, q^2-1) for Y in [y3, y6, y9, y12, y15]]
    if verbose == 1: print("Random point Q: [e3, e6, e9, e12, e15] =", SOL)
    if verbose == 2: print("Random point Q:", Q)

    return Q






def lift_y(y):
    """
    Input:  An element y of K = k(t)
    Output: List of rational points of E with y-coordinate equal to y (the list can be empty if no such point exists)
    """
    S.<x> = PolynomialRing(K)
    poly = S(x^3 + b*x + K(t^(q+1)) - y^2)
    xs = poly.roots(K, multiplicities=False)
    return [E(x, y) for x in xs]


def power(g, ei):
    """
    Input:  A generator g of k^\times and an item ei which is either an integer or the empty string ""
    Output: Return g^ei if ei is an integer, and return 0 otherwise.
    """
    if ei == '': return 0
    return g^ei



def expo_into_element(L):
    """
    Input:  A list L of items which are either integers of the empty string
    Output: Return the list [g^e for e in L], where g^e := 0 if e = "" (the empty string)
    """
    #Use it like [y3, y6, y9, y12, y15] = expo_into_element(L)
    return [power(g, e) for e in L]



def is_indeed_point(L):
    """
    Input:  A list L of 5 items [e3, e6, e9, e12, e15] which are either integers of the empty string, corresponding the the discrete logarithms of y3, y6, y9, y12, y15 in base g. (The discrete log is set to be the empty string "" if the coefficient is 0).
    Output: Returns a rational point (x, y) on E such that y_i = g^ei for i = 3, 6, 9, 12, 15.
    """
    [y3, y6, y9, y12, y15] = expo_into_element(L)

    y0 = (-y12^5*y15^8 + y9*y12^3*y15^9 - y6*y9*y15^11 - y3*y12*y15^11 + y9*y12*y15^5 + y6*y15^6 - y12)/y15^12
    y1 = (-y12^4*y15^8 - y9^2*y15^10 + y6*y12*y15^10 + y3*y15^11 + y12^2*y15^4 - y9*y15^5 - 1)/y15^13
    y2 = (-y12^3)/y15^6
    y4 = (y12^3*y15^4 + y9*y12*y15^5 + y6*y15^6 - y12)/y15^8
    y5 = 1/y15^9
    y7 = (-y12^2*y15^4 + y9*y15^5 + 1)/y15^7
    y8 = 0
    y10 = y12/y15^2
    y11 = 1/y15^3
    y13 = (-1)/y15
    y14 = 0

    Py = K(y0 + y1*t^1 + y2*t^2 + y3*t^3 + y4*t^4 + y5*t^5 + y6*t^6 + y7*t^7 + y8*t^8 + y9*t^9 + y10*t^10 + y11*t^11 + y12*t^12 + y13*t^13 + y14*t^14 + y15*t^15)

    list_points = lift_y(Py)

    return list_points[0]



def discretelog(x, g, order):
    """
    Input:  An element x of k, a generator of k^\times of order "order"
    Output: Returns the discrete logarithm of x in base g if x != 0, and returns the empty string "" if x = 0.
    """
    if x == 0: return ""
    return discrete_log(x, g, order, operation='*')


def print_point_x(P):
    """
    Input:  A rational point P on E such that the x-coordinate x(P) is a *polynomial*
    Output: Returns the list of discrete logarithms of the coefficients of x in base g (from x_0 to x_15)
    """
    return [discretelog(xi, g, q^2 - 1) for xi in R(P[0]).coefficients(sparse=False)]


def print_point_y(P):
    """
    Input:  A rational point P on E such that the y-coordinate y(P) is a *polynomial*
    Output: Returns the list of discrete logarithms of the coefficients of y in base g (from y_0 to y_10)
    """
    return [discretelog(xi, g, q^2 - 1) for xi in R(P[1]).coefficients(sparse=False)]













#The following list consists of 54 polynomials of degree 10 over k, and they are x-coordinates of rational points of minimal height on E(K).
basis_0_x_coord = [t^10 + t^8 + t^2, t^10 + t^8 + t^2 + 2*z^5 + 2*z^3 + z^2 + 2*z, (z^5 + z^4 + 2*z^3 + 2*z^2 + z + 1)*t^10 + (2*z^5 + z^3 + 1)*t^8 + (2*z^5 + z^4 + 2*z^3 + 2*z + 2)*t^2, (z^5 + z^4 + 2*z^3 + 2*z^2 + z + 1)*t^10 + (2*z^5 + z^3 + 1)*t^8 + (2*z^5 + z^4 + 2*z^3 + 2*z + 2)*t^2 + 2*z^5 + 2*z^3 + z^2 + 2*z, (2*z^5 + 2*z^4)*t^10 + (z^4 + 2*z^3)*t^8 + (z^5 + z^4 + 2)*t^7 + (z^5 + z^4 + 2)*t^6 + (z^4 + 2*z^3 + 2)*t^5 + t^4 + (z^4 + 2*z^3 + 2)*t^3 + (z^5 + z^3 + 1)*t^2 + (2*z^4 + z^3 + 1)*t + z^4 + 2*z^3 + 2*z^2 + z + 2, (2*z^5 + 2*z^4)*t^10 + (z^4 + 2*z^3)*t^8 + (z^5 + z^4 + 2)*t^7 + (z^5 + z^4 + 2)*t^6 + (z^4 + 2*z^3 + 2)*t^5 + t^4 + (z^4 + 2*z^3 + 2)*t^3 + (z^5 + z^3 + 1)*t^2 + (2*z^4 + z^3 + 1)*t + 2*z^5 + z^4 + z^3 + 2, (z^4 + 1)*t^10 + (2*z^5 + z^4 + z^3 + 2*z)*t^8 + (2*z^5 + z^4 + 2*z^3 + z^2 + z + 1)*t^2, (z^4 + 1)*t^10 + (2*z^5 + z^4 + z^3 + 2*z)*t^8 + (2*z^5 + z^4 + 2*z^3 + z^2 + z + 1)*t^2 + 2*z^5 + 2*z^3 + z^2 + 2*z, (z^5 + 2*z^4 + z^3 + z + 1)*t^10 + (z^5 + z^4 + 2*z^3 + 2*z^2 + 2*z)*t^8 + (z^2 + z + 1)*t^2, (z^5 + 2*z^4 + z^3 + z + 1)*t^10 + (z^5 + z^4 + 2*z^3 + 2*z^2 + 2*z)*t^8 + (z^2 + z + 1)*t^2 + 2*z^5 + 2*z^3 + z^2 + 2*z, (2*z^5 + 2*z^4 + 2)*t^10 + (2*z^5 + 2*z^3)*t^8 + (z^5 + z^4)*t^7 + (z^5 + z^4)*t^6 + (2*z^5 + 2*z^3 + 2)*t^5 + t^4 + (2*z^5 + 2*z^3 + 2)*t^3 + (2*z^5 + z^4 + z^3 + 2)*t^2 + (z^5 + z^3 + 1)*t + 2*z^4 + z^3 + 1, (2*z^5 + 2*z^4 + 2)*t^10 + (2*z^5 + 2*z^3)*t^8 + (z^5 + z^4)*t^7 + (z^5 + z^4)*t^6 + (2*z^5 + 2*z^3 + 2)*t^5 + t^4 + (2*z^5 + 2*z^3 + 2)*t^3 + (2*z^5 + z^4 + z^3 + 2)*t^2 + (z^5 + z^3 + 1)*t + 2*z^5 + 2*z^4 + z^2 + 2*z + 1, (2*z^4 + z^3 + z + 2)*t^10 + (z^5 + 2*z^4 + z^3 + 2*z^2 + 2*z + 2)*t^8 + (z^5 + 2*z^3 + 2)*t^2, (2*z^4 + z^3 + z + 2)*t^10 + (z^5 + 2*z^4 + z^3 + 2*z^2 + 2*z + 2)*t^8 + (z^5 + 2*z^3 + 2)*t^2 + 2*z^5 + 2*z^3 + z^2 + 2*z, (z^4 + 2*z^3 + 2*z^2 + 2*z)*t^10 + (z^4 + 2*z^3 + 2*z + 1)*t^8 + (z^5 + z^4 + 2*z^3 + 2*z^2 + z + 1)*t^2, (z^4 + 2*z^3 + 2*z^2 + 2*z)*t^10 + (z^4 + 2*z^3 + 2*z + 1)*t^8 + (z^5 + z^4 + 2*z^3 + 2*z^2 + z + 1)*t^2 + 2*z^5 + 2*z^3 + z^2 + 2*z, t^10 + (2*z^5 + 2*z^3 + z^2 + 2*z + 1)*t^9 + (z^5 + z^3 + 2*z^2 + z)*t^8 + t^7 + t^6 + (2*z^5 + 2*z^3 + z^2 + 2*z)*t^5 + 2*t^4 + (2*z^5 + 2*z^3 + z^2 + 2*z + 2)*t^3 + 2*t^2, t^10 + (2*z^5 + 2*z^3 + z^2 + 2*z + 1)*t^9 + (z^5 + z^3 + 2*z^2 + z)*t^8 + t^7 + t^6 + (2*z^5 + 2*z^3 + z^2 + 2*z)*t^5 + 2*t^4 + (2*z^5 + 2*z^3 + z^2 + 2*z + 2)*t^3 + 2*t^2 + 2*z^5 + 2*z^3 + z^2 + 2*z, t^10 + (2*z^5 + 2*z^3 + z^2 + 2*z)*t^9 + (2*z^5 + 2*z^3 + z^2 + 2*z)*t^8 + (2*z^5 + 2*z^3 + z^2 + 2*z + 2)*t^7 + (2*z^5 + 2*z^3 + z^2 + 2*z + 2)*t^6 + (z^5 + z^3 + 2*z^2 + z)*t^5 + (z^5 + z^3 + 2*z^2 + z + 1)*t^4 + 2*t^3 + 2*t^2, t^10 + (2*z^5 + 2*z^3 + z^2 + 2*z)*t^9 + (2*z^5 + 2*z^3 + z^2 + 2*z)*t^8 + (2*z^5 + 2*z^3 + z^2 + 2*z + 2)*t^7 + (2*z^5 + 2*z^3 + z^2 + 2*z + 2)*t^6 + (z^5 + z^3 + 2*z^2 + z)*t^5 + (z^5 + z^3 + 2*z^2 + z + 1)*t^4 + 2*t^3 + 2*t^2 + 2*z^5 + 2*z^3 + z^2 + 2*z, t^10 + (z^5 + z^3 + 2*z^2 + z + 1)*t^9 + (2*z^5 + 2*z^3 + z^2 + 2*z)*t^8 + t^7 + t^6 + (z^5 + z^3 + 2*z^2 + z)*t^5 + 2*t^4 + (z^5 + z^3 + 2*z^2 + z + 2)*t^3 + 2*t^2, t^10 + (z^5 + z^3 + 2*z^2 + z + 1)*t^9 + (2*z^5 + 2*z^3 + z^2 + 2*z)*t^8 + t^7 + t^6 + (z^5 + z^3 + 2*z^2 + z)*t^5 + 2*t^4 + (z^5 + z^3 + 2*z^2 + z + 2)*t^3 + 2*t^2 + 2*z^5 + 2*z^3 + z^2 + 2*z, t^10 + (z^5 + z^3 + 2*z^2 + z + 2)*t^9 + 2*t^8 + (z^5 + z^3 + 2*z^2 + z + 1)*t^7 + (z^5 + z^3 + 2*z^2 + z + 1)*t^6 + (z^5 + z^3 + 2*z^2 + z + 1)*t^5 + (z^5 + z^3 + 2*z^2 + z + 1)*t^4 + (2*z^5 + 2*z^3 + z^2 + 2*z)*t^3 + (z^5 + z^3 + 2*z^2 + z + 2)*t^2 + (z^5 + z^3 + 2*z^2 + z + 2)*t + 1, t^10 + (z^5 + z^3 + 2*z^2 + z + 2)*t^9 + 2*t^8 + (z^5 + z^3 + 2*z^2 + z + 1)*t^7 + (z^5 + z^3 + 2*z^2 + z + 1)*t^6 + (z^5 + z^3 + 2*z^2 + z + 1)*t^5 + (z^5 + z^3 + 2*z^2 + z + 1)*t^4 + (2*z^5 + 2*z^3 + z^2 + 2*z)*t^3 + (z^5 + z^3 + 2*z^2 + z + 2)*t^2 + (z^5 + z^3 + 2*z^2 + z + 2)*t + 2*z^5 + 2*z^3 + z^2 + 2*z + 1, t^10 + (z^5 + z^3 + 2*z^2 + z)*t^9 + (z^5 + z^3 + 2*z^2 + z)*t^8 + (z^5 + z^3 + 2*z^2 + z + 2)*t^7 + (z^5 + z^3 + 2*z^2 + z + 2)*t^6 + (2*z^5 + 2*z^3 + z^2 + 2*z)*t^5 + (2*z^5 + 2*z^3 + z^2 + 2*z + 1)*t^4 + 2*t^3 + 2*t^2, t^10 + (z^5 + z^3 + 2*z^2 + z)*t^9 + (z^5 + z^3 + 2*z^2 + z)*t^8 + (z^5 + z^3 + 2*z^2 + z + 2)*t^7 + (z^5 + z^3 + 2*z^2 + z + 2)*t^6 + (2*z^5 + 2*z^3 + z^2 + 2*z)*t^5 + (2*z^5 + 2*z^3 + z^2 + 2*z + 1)*t^4 + 2*t^3 + 2*t^2 + 2*z^5 + 2*z^3 + z^2 + 2*z, t^10 + (2*z^5 + 2*z^3 + z^2 + 2*z + 2)*t^9 + 2*t^8 + (2*z^5 + 2*z^3 + z^2 + 2*z + 1)*t^7 + (2*z^5 + 2*z^3 + z^2 + 2*z + 1)*t^6 + (2*z^5 + 2*z^3 + z^2 + 2*z + 1)*t^5 + (2*z^5 + 2*z^3 + z^2 + 2*z + 1)*t^4 + (z^5 + z^3 + 2*z^2 + z)*t^3 + (2*z^5 + 2*z^3 + z^2 + 2*z + 2)*t^2 + (2*z^5 + 2*z^3 + z^2 + 2*z + 2)*t + 1, t^10 + (2*z^5 + 2*z^3 + z^2 + 2*z + 2)*t^9 + 2*t^8 + (2*z^5 + 2*z^3 + z^2 + 2*z + 1)*t^7 + (2*z^5 + 2*z^3 + z^2 + 2*z + 1)*t^6 + (2*z^5 + 2*z^3 + z^2 + 2*z + 1)*t^5 + (2*z^5 + 2*z^3 + z^2 + 2*z + 1)*t^4 + (z^5 + z^3 + 2*z^2 + z)*t^3 + (2*z^5 + 2*z^3 + z^2 + 2*z + 2)*t^2 + (2*z^5 + 2*z^3 + z^2 + 2*z + 2)*t + 2*z^5 + 2*z^3 + z^2 + 2*z + 1, (2*z^3 + z^2 + 2)*t^10 + (z^5 + z^4 + 2*z^3 + z^2 + z)*t^9 + (2*z^5 + z^4 + z^3 + 2*z^2 + 1)*t^8 + (2*z^5 + z^4 + 2*z^3 + 2*z^2 + 2)*t^7 + (2*z^5 + z^4 + 2*z^3 + 2*z^2 + 2)*t^6 + (z^5 + 2*z^3 + 2*z^2 + 2*z)*t^5 + (z^5 + 2*z^4 + 2*z^3 + 2*z^2 + z)*t^4 + (2*z^3 + z^2 + 2)*t^3 + (2*z^5 + z^4 + z^3 + z^2 + 2*z + 2)*t^2 + (2*z^5 + z^2)*t + z^3 + 2*z^2 + 2*z + 2, (2*z^3 + z^2 + 2)*t^10 + (z^5 + z^4 + 2*z^3 + z^2 + z)*t^9 + (2*z^5 + z^4 + z^3 + 2*z^2 + 1)*t^8 + (2*z^5 + z^4 + 2*z^3 + 2*z^2 + 2)*t^7 + (2*z^5 + z^4 + 2*z^3 + 2*z^2 + 2)*t^6 + (z^5 + 2*z^3 + 2*z^2 + 2*z)*t^5 + (z^5 + 2*z^4 + 2*z^3 + 2*z^2 + z)*t^4 + (2*z^3 + z^2 + 2)*t^3 + (2*z^5 + z^4 + z^3 + z^2 + 2*z + 2)*t^2 + (2*z^5 + z^2)*t + 2*z^5 + z + 2, (2*z^3 + z^2 + 2)*t^10 + (2*z^5 + 2*z^2 + z)*t^9 + (2*z^5 + 2*z^2 + 2)*t^8 + (2*z^5 + 2*z^4 + z^3 + 2*z^2)*t^7 + (2*z^5 + 2*z^4 + z^3 + 2*z^2)*t^6 + (z^5 + z + 1)*t^5 + z^4*t^4 + (2*z^5 + 2*z^4 + z^3 + z^2)*t^3 + (2*z^3 + 2*z^2 + 2*z + 2)*t^2 + (2*z^5 + z^4 + z + 1)*t + 2*z^4 + z^3 + z^2 + z, (2*z^3 + z^2 + 2)*t^10 + (2*z^5 + 2*z^2 + z)*t^9 + (2*z^5 + 2*z^2 + 2)*t^8 + (2*z^5 + 2*z^4 + z^3 + 2*z^2)*t^7 + (2*z^5 + 2*z^4 + z^3 + 2*z^2)*t^6 + (z^5 + z + 1)*t^5 + z^4*t^4 + (2*z^5 + 2*z^4 + z^3 + z^2)*t^3 + (2*z^3 + 2*z^2 + 2*z + 2)*t^2 + (2*z^5 + z^4 + z + 1)*t + 2*z^5 + 2*z^4 + 2*z^2, (2*z^3 + z^2 + 2)*t^10 + (2*z^3 + z^2 + z + 1)*t^9 + (2*z^5 + z^4 + z^3 + 2*z^2 + z + 1)*t^8 + (2*z^5 + z^2 + 2*z)*t^7 + (2*z^5 + z^2 + 2*z)*t^6 + (z^5 + 2*z^3 + 2*z^2 + z + 1)*t^5 + (2*z^4 + 2*z^3 + z + 1)*t^4 + (z^5 + 2*z^4 + z^3 + 2*z^2 + 2*z + 1)*t^3 + (2*z^5 + z^4 + 2*z^2 + 2*z + 2)*t^2 + (z^5 + 2*z^4 + z^3 + 1)*t + z^3 + 2*z^2 + z, (2*z^3 + z^2 + 2)*t^10 + (2*z^3 + z^2 + z + 1)*t^9 + (2*z^5 + z^4 + z^3 + 2*z^2 + z + 1)*t^8 + (2*z^5 + z^2 + 2*z)*t^7 + (2*z^5 + z^2 + 2*z)*t^6 + (z^5 + 2*z^3 + 2*z^2 + z + 1)*t^5 + (2*z^4 + 2*z^3 + z + 1)*t^4 + (z^5 + 2*z^4 + z^3 + 2*z^2 + 2*z + 1)*t^3 + (2*z^5 + z^4 + 2*z^2 + 2*z + 2)*t^2 + (z^5 + 2*z^4 + z^3 + 1)*t + 2*z^5, (2*z^3 + z^2 + 2)*t^10 + (2*z^5 + z^4 + 2*z^3 + z^2)*t^9 + (2*z^5 + 2*z^2 + 2*z + 2)*t^8 + (2*z^3 + 2*z^2 + 1)*t^7 + (2*z^3 + 2*z^2 + 1)*t^6 + (z^5 + z^3)*t^5 + (2*z^5 + 2*z^4 + 2*z^3 + 2*z^2)*t^4 + (2*z^4 + z^3 + 2*z^2 + 2*z)*t^3 + (z^4 + 2)*t^2 + (z^5 + z^3 + 2*z)*t + z^4 + z^2 + 1, (2*z^3 + z^2 + 2)*t^10 + (2*z^5 + z^4 + 2*z^3 + z^2)*t^9 + (2*z^5 + 2*z^2 + 2*z + 2)*t^8 + (2*z^3 + 2*z^2 + 1)*t^7 + (2*z^3 + 2*z^2 + 1)*t^6 + (z^5 + z^3)*t^5 + (2*z^5 + 2*z^4 + 2*z^3 + 2*z^2)*t^4 + (2*z^4 + z^3 + 2*z^2 + 2*z)*t^3 + (z^4 + 2)*t^2 + (z^5 + z^3 + 2*z)*t + 2*z^5 + z^4 + 2*z^3 + 2*z^2 + 2*z + 1, (2*z^3 + z^2 + 2)*t^10 + (2*z^3 + 1)*t^9 + (2*z^5 + 2*z^2 + z + 2)*t^8 + (2*z^5 + 1)*t^7 + (2*z^5 + 1)*t^6 + (2*z^5 + z^4)*t^5 + (z^3 + 2*z^2 + 1)*t^4 + (z^5 + z^4 + z^3 + 2*z^2 + 2*z + 2)*t^3 + (2*z^5 + 2*z^4 + 2*z^3 + z^2 + z + 1)*t^2 + (2*z^5 + 2*z^4 + z^2 + 2*z + 2)*t + 2*z^4 + z^3 + z + 2, (2*z^3 + z^2 + 2)*t^10 + (2*z^3 + 1)*t^9 + (2*z^5 + 2*z^2 + z + 2)*t^8 + (2*z^5 + 1)*t^7 + (2*z^5 + 1)*t^6 + (2*z^5 + z^4)*t^5 + (z^3 + 2*z^2 + 1)*t^4 + (z^5 + z^4 + z^3 + 2*z^2 + 2*z + 2)*t^3 + (2*z^5 + 2*z^4 + 2*z^3 + z^2 + z + 1)*t^2 + (2*z^5 + 2*z^4 + z^2 + 2*z + 2)*t + 2*z^5 + 2*z^4 + z^2 + 2, (2*z^3 + z^2 + 2)*t^10 + (2*z^5 + 2*z^4 + 2*z^3 + z^2 + 2*z)*t^9 + (2*z^5 + z^4 + z^3 + 2*z^2 + 1)*t^8 + (2*z^3 + 2)*t^7 + (2*z^3 + 2)*t^6 + (z^5 + z^4 + 2*z^3 + z + 1)*t^5 + (2*z^4 + 2*z^3 + 2*z + 2)*t^4 + (z^5 + z^4 + 2*z^3 + z^2 + 1)*t^3 + (2*z^5 + 2*z^4 + z^3 + 2*z^2 + z)*t^2 + (2*z^4 + z^3 + 2*z^2 + 1)*t + z^4 + z^3 + z, (2*z^3 + z^2 + 2)*t^10 + (2*z^5 + 2*z^4 + 2*z^3 + z^2 + 2*z)*t^9 + (2*z^5 + z^4 + z^3 + 2*z^2 + 1)*t^8 + (2*z^3 + 2)*t^7 + (2*z^3 + 2)*t^6 + (z^5 + z^4 + 2*z^3 + z + 1)*t^5 + (2*z^4 + 2*z^3 + 2*z + 2)*t^4 + (z^5 + z^4 + 2*z^3 + z^2 + 1)*t^3 + (2*z^5 + 2*z^4 + z^3 + 2*z^2 + z)*t^2 + (2*z^4 + z^3 + 2*z^2 + 1)*t + 2*z^5 + z^4 + z^2, (2*z^3 + z^2 + 2)*t^10 + (z^5 + z^3 + 2*z)*t^9 + (2*z^5 + 2*z^2 + 2)*t^8 + (z^4 + 2*z^3 + 2)*t^7 + (z^4 + 2*z^3 + 2)*t^6 + (z^5 + 2*z^2 + 2*z + 1)*t^5 + (2*z^5 + 2*z^4 + 2*z^3 + 2*z + 1)*t^4 + (z^4 + z^3 + 2*z^2 + 2*z + 2)*t^3 + (2*z^3 + z^2 + 2)*t^2 + (2*z^5 + 2*z^4 + 2*z^3 + z^2 + 2*z + 1)*t + z^4 + z^3 + 2*z^2 + 2*z + 1, (2*z^3 + z^2 + 2)*t^10 + (z^5 + z^3 + 2*z)*t^9 + (2*z^5 + 2*z^2 + 2)*t^8 + (z^4 + 2*z^3 + 2)*t^7 + (z^4 + 2*z^3 + 2)*t^6 + (z^5 + 2*z^2 + 2*z + 1)*t^5 + (2*z^5 + 2*z^4 + 2*z^3 + 2*z + 1)*t^4 + (z^4 + z^3 + 2*z^2 + 2*z + 2)*t^3 + (2*z^3 + z^2 + 2)*t^2 + (2*z^5 + 2*z^4 + 2*z^3 + z^2 + 2*z + 1)*t + 2*z^5 + z^4 + z + 1, (2*z^3 + z^2 + 2)*t^10 + (2*z^3 + z^2 + 2*z + 2)*t^9 + (2*z^5 + z^4 + z^3 + 2*z^2 + z + 1)*t^8 + (z^4 + z^3 + z^2 + 2*z + 1)*t^7 + (z^4 + z^3 + z^2 + 2*z + 1)*t^6 + (z^5 + z^4 + 2*z^3)*t^5 + (2*z^5 + z^3 + 2*z^2 + 2*z + 1)*t^4 + (z^5 + 2*z^3 + z + 2)*t^3 + (2*z^5 + 2*z^4 + z + 1)*t^2 + (2*z^5 + z^4 + z^3 + z^2 + z + 2)*t + 2*z^4 + z^3 + z^2 + 2*z, (2*z^3 + z^2 + 2)*t^10 + (2*z^3 + z^2 + 2*z + 2)*t^9 + (2*z^5 + z^4 + z^3 + 2*z^2 + z + 1)*t^8 + (z^4 + z^3 + z^2 + 2*z + 1)*t^7 + (z^4 + z^3 + z^2 + 2*z + 1)*t^6 + (z^5 + z^4 + 2*z^3)*t^5 + (2*z^5 + z^3 + 2*z^2 + 2*z + 1)*t^4 + (z^5 + 2*z^3 + z + 2)*t^3 + (2*z^5 + 2*z^4 + z + 1)*t^2 + (2*z^5 + z^4 + z^3 + z^2 + z + 2)*t + 2*z^5 + 2*z^4 + 2*z^2 + z, (2*z^3 + z^2 + 2)*t^10 + (2*z^5 + z^4 + 2*z^3 + 2*z^2)*t^9 + (2*z^5 + z^4 + z^3 + 2*z^2 + 1)*t^8 + (2*z^5 + z^4 + 2*z^3 + 2*z^2 + 2)*t^7 + (2*z^5 + z^4 + 2*z^3 + 2*z^2 + 2)*t^6 + (z^5 + z^4 + 2*z^3 + z^2 + 2)*t^5 + (2*z^5 + 2*z^4 + z^2 + 1)*t^4 + (2*z^4 + z^2 + 2*z)*t^3 + (z^5 + z^4 + 2*z^3 + 2*z + 1)*t^2 + (z^5 + 2*z^2 + 2*z + 1)*t + z^3, (2*z^3 + z^2 + 2)*t^10 + (2*z^5 + z^4 + 2*z^3 + 2*z^2)*t^9 + (2*z^5 + z^4 + z^3 + 2*z^2 + 1)*t^8 + (2*z^5 + z^4 + 2*z^3 + 2*z^2 + 2)*t^7 + (2*z^5 + z^4 + 2*z^3 + 2*z^2 + 2)*t^6 + (z^5 + z^4 + 2*z^3 + z^2 + 2)*t^5 + (2*z^5 + 2*z^4 + z^2 + 1)*t^4 + (2*z^4 + z^2 + 2*z)*t^3 + (z^5 + z^4 + 2*z^3 + 2*z + 1)*t^2 + (z^5 + 2*z^2 + 2*z + 1)*t + 2*z^5 + z^2 + 2*z, (2*z^3 + z^2 + 2)*t^10 + (2*z^5 + z^3 + z)*t^9 + (2*z^5 + 2*z^2 + 2*z + 2)*t^8 + (2*z^5 + z^3 + 2*z + 1)*t^7 + (2*z^5 + z^3 + 2*z + 1)*t^6 + (2*z^5 + 2*z^4 + z^3 + 1)*t^5 + (2*z^5 + 2*z^2 + 1)*t^4 + (2*z^4 + z^3 + z + 2)*t^3 + (z^3 + 2*z^2 + 1)*t^2 + (2*z^5 + z^4 + 2*z^3 + 2*z + 2)*t + z^4 + z^3 + z^2 + 2, (2*z^3 + z^2 + 2)*t^10 + (2*z^5 + z^3 + z)*t^9 + (2*z^5 + 2*z^2 + 2*z + 2)*t^8 + (2*z^5 + z^3 + 2*z + 1)*t^7 + (2*z^5 + z^3 + 2*z + 1)*t^6 + (2*z^5 + 2*z^4 + z^3 + 1)*t^5 + (2*z^5 + 2*z^2 + 1)*t^4 + (2*z^4 + z^3 + z + 2)*t^3 + (z^3 + 2*z^2 + 1)*t^2 + (2*z^5 + z^4 + 2*z^3 + 2*z + 2)*t + 2*z^5 + z^4 + 2*z^2 + 2*z + 2, (2*z^3 + z^2 + 2)*t^10 + (z^5 + 2*z^4 + 2*z^3 + z^2)*t^9 + (2*z^5 + 2*z^2 + 2*z + 2)*t^8 + (2*z^5 + z^3 + 2*z + 1)*t^7 + (2*z^5 + z^3 + 2*z + 1)*t^6 + (z^5 + 2*z^3 + 2*z^2 + 2*z + 2)*t^5 + (2*z^4 + 2*z^3 + 2*z^2 + 2*z + 1)*t^4 + (2*z^5 + 2*z^4 + 2*z^2 + 2)*t^3 + (z^4 + z^3 + 2*z^2 + 2*z + 1)*t^2 + 2*z^4*t + z^4 + 2*z^3 + z + 2, (2*z^3 + z^2 + 2)*t^10 + (z^5 + 2*z^4 + 2*z^3 + z^2)*t^9 + (2*z^5 + 2*z^2 + 2*z + 2)*t^8 + (2*z^5 + z^3 + 2*z + 1)*t^7 + (2*z^5 + z^3 + 2*z + 1)*t^6 + (z^5 + 2*z^3 + 2*z^2 + 2*z + 2)*t^5 + (2*z^4 + 2*z^3 + 2*z^2 + 2*z + 1)*t^4 + (2*z^5 + 2*z^4 + 2*z^2 + 2)*t^3 + (z^4 + z^3 + 2*z^2 + 2*z + 1)*t^2 + 2*z^4*t + 2*z^5 + z^4 + z^3 + z^2 + 2, (2*z^3 + z^2 + 2)*t^10 + (2*z^3 + 2*z^2 + 2)*t^9 + (2*z^5 + 2*z^2 + z + 2)*t^8 + (2*z^2 + z + 1)*t^7 + (2*z^2 + z + 1)*t^6 + (2*z^4 + 2*z^2 + z + 2)*t^5 + (z^5 + 2*z^4 + z^3 + z^2 + z + 2)*t^4 + (z^4 + z^3 + z^2 + 2*z + 2)*t^3 + (2*z^3 + 2*z)*t^2 + (2*z^5 + z^4 + 2*z^3 + 2*z^2 + 2)*t + 2*z^4 + 2*z^3 + z^2 + 2, (2*z^3 + z^2 + 2)*t^10 + (2*z^3 + 2*z^2 + 2)*t^9 + (2*z^5 + 2*z^2 + z + 2)*t^8 + (2*z^2 + z + 1)*t^7 + (2*z^2 + z + 1)*t^6 + (2*z^4 + 2*z^2 + z + 2)*t^5 + (z^5 + 2*z^4 + z^3 + z^2 + z + 2)*t^4 + (z^4 + z^3 + z^2 + 2*z + 2)*t^3 + (2*z^3 + 2*z)*t^2 + (2*z^5 + z^4 + 2*z^3 + 2*z^2 + 2)*t + 2*z^5 + 2*z^4 + z^3 + 2*z^2 + 2*z + 2, (2*z^3 + z^2 + 2)*t^10 + (z^5 + 2*z^2 + 2*z)*t^9 + (2*z^5 + 2*z^2 + 2*z + 2)*t^8 + (2*z^3 + 2*z^2 + 1)*t^7 + (2*z^3 + 2*z^2 + 1)*t^6 + (z^4 + 2*z^3 + 2*z^2 + 2*z + 1)*t^5 + (z^4 + z^3 + z^2 + 1)*t^4 + (2*z^5 + 2*z^4 + 2*z + 1)*t^3 + (z^5 + 2*z^4 + 2*z^3 + z^2 + 2*z + 1)*t^2 + (z^5 + z^4 + z^2 + z)*t + z^2 + 2*z + 2, (2*z^3 + z^2 + 2)*t^10 + (z^5 + 2*z^2 + 2*z)*t^9 + (2*z^5 + 2*z^2 + 2*z + 2)*t^8 + (2*z^3 + 2*z^2 + 1)*t^7 + (2*z^3 + 2*z^2 + 1)*t^6 + (z^4 + 2*z^3 + 2*z^2 + 2*z + 1)*t^5 + (z^4 + z^3 + z^2 + 1)*t^4 + (2*z^5 + 2*z^4 + 2*z + 1)*t^3 + (z^5 + 2*z^4 + 2*z^3 + z^2 + 2*z + 1)*t^2 + (z^5 + z^4 + z^2 + z)*t + 2*z^5 + 2*z^3 + 2*z^2 + z + 2]

#This is a list of 54 rational points in E(K) with minimal height. They are checked to be linearly independent over R in the main file.
basis_0 = [E.lift_x(Px) for Px in basis_0_x_coord]


