This folder contains 4 files (plus this "read me"), related to the PhD thesis "Some Mordell-Weil lattices and applications to sphere packings".

The 4 files can be run on SAGE to compile and test the algorithms (for instance, by uploading these files on cocalc.com).


1) The file "Explanation of the formulas.ipynb" contains pre-computations that explain the formulas used in the other 3 files.

2) The file "Computation kissing number of L'_{3,1}.ipynb" contains the main code that computes the kissing number of the 54-dimensional narrow Mordell-Weil lattice of y^2 = x^3 + b x + t^{3^n + 1} over F_{3^{2n}}(t) where n = 3 and b = 1.

3) The file "Useful_functions_computation_Gram_matrix.sage" contains the code for the functions used in "Computation Gram matrix of L'_{3, 1}.ipynb".

4) The file "Computation Gram matrix of L'_{3, 1}.ipynb" contains the main code that computes a Gram matrix for (sublattices of) L'_{3, 1}.


Author:	Gauthier LETERRIER
Date:	2022-12-03 (YYYY-MM-DD)
Contact:	gauthier (dot) leterrier (at) gmail (dot) com